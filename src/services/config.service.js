export const CONFIG = {
  DurationOfToken: 1/120, // hours
  apiUrl:  'http://nestapisd.herokuapp.com',
  httpErrorTitle: 'Error',
  httpErrorContent: 'Request failure, please contact with your administration!',
  baseURL: "http://localhost:9000/",
  baseURLAuth: "https://nestapisd.herokuapp.com/"
}
