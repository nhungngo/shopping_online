import { http } from './http.service';
import { LOG_OUT } from '../actions/actions';
import { CONFIG } from '../services/config.service';
import { store } from '../App';

export class Authentication {
  static storeAuth(
    expiredIn,
    token,
    refreshToken,
    email = localStorage.getItem('email')
  ) {
    localStorage.setItem('expiredIn', expiredIn);
    localStorage.setItem('token', token);
    localStorage.setItem('refreshToken', refreshToken);
    localStorage.setItem('email', email);
  }

  static deleteAuth = () => {};

  static isLoggin = () => {
    return localStorage.getItem('token') ? true : false;
  };

  static logout = () => {
    localStorage.clear();
    store.dispatch({ type: LOG_OUT });
  };

  /**
   * @param email: string
   * @param refreshToken: string
   * model:
      {
        email: "admin123@gmail.com"
        refreshToken: "afadfafa" (not required)
      }
   */
  static refreshToken = (email, refreshToken) => {
    const data = {
      email: email,
      refreshToken: refreshToken
    };
    return http.post('/user/refreshToken', data, {}, CONFIG.baseURLAuth);
  };

  /**
   * @param userInfo: object
   * model:
      {
        address1: "England"
        address2: "London" (not required)
        agreement: true
        birthday: "2019-04-18"
        city: "London"
        company: "Nashtech" (not required)
        confirm: "Nashtech@123"
        country: "USA"
        email: "nashtech@gmail.com"
        firstname: "lady"
        gender: "male"
        lastname: "gaga"
        password: "Nashtech@123"
        confirmpassword: "Nashtech@123"
        phone: "0965067698"
        postcode: "100000"
        regionstate: "USA"
      }
   */
  static register(userInfo) {
    return http.post('/user/register', userInfo, {}, CONFIG.baseURLAuth);
  }

  /**
   * @param user: object
   * model:
      {
        username: "string",
        password: "string"
      }
   */
  static login(user) {
    return http.post('/user/login', user, {}, CONFIG.baseURLAuth);
  }
}
