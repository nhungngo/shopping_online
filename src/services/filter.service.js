export class FilterService {
  static filterProduct = (filter, productList) => {
    let avai = filter.not_available;
    let stock = filter.in_stock;
    let price_from = filter.price_from;
    let price_to = filter.price_to;
    let filteredProduct;

    if (!avai) {
      if (!stock) {
        filteredProduct = productList;
      } else {
        filteredProduct = productList.filter(product => {
          return product.availability === 'in stock';
        });
      }
    }
    if (avai) {
      if (stock) {
        filteredProduct = productList.filter(product => {
          return (
            product.availability === 'in stock' ||
            product.availability === 'not available'
          );
        });
      } else {
        filteredProduct = productList.filter(product => {
          return product.availability === 'not available';
        });
      }
    }
    return filteredProduct.filter(product => {
      return product.cost >= price_from && product.cost <= price_to;
    });
  };
  static sortProduct = (display, productList) => {
    let sortedProduct = [];
    let sort = display.sort;
    switch (sort) {
      case 'default': {
        sortedProduct = productList;
        break;
      }
      case 'ascName': {
        sortedProduct = productList.sort((a, b) =>
          a.productName.localeCompare(b.productName)
        );
        break;
      }
      case 'desName': {
        sortedProduct = productList.sort((a, b) =>
          b.productName.localeCompare(a.productName)
        );
        break;
      }
      case 'ascPrice': {
        sortedProduct = productList.sort((a, b) => a.cost - b.cost);
        break;
      }
      case 'desPrice': {
        sortedProduct = productList.sort((a, b) => b.cost - a.cost);
        break;
      }
      default:
        sortedProduct = productList;
    }
    return sortedProduct;
  };
  static showProduct = (current, pageSize, sortedProduct) => {
    return sortedProduct.slice((current - 1) * pageSize, current * pageSize);
  };
  static search = (productList, searchKey) => {
    return productList.filter(p => {
      return (
        String(p.productName)
          .toLowerCase()
          .search(searchKey.toLowerCase()) !== -1
      );
    });
  };
}
