import axios from 'axios';
import {
  store
} from '../App';
import {
  from
} from "rxjs";
import {
  switchMap
} from "rxjs/operators";
import {
  SHOW_LOADING,
  HIDE_LOADING,
  SHOW_PROMPT
} from '../actions/actions';
import {
  CONFIG
} from "../services/config.service";
// import { Authentication } from "./authen.service";

const getHeaders = () => {
  return {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${localStorage.getItem('token')}`,
  }
}

const instance = axios.create({
  baseURL: CONFIG.baseURL,
  headers: {
    'Content-Type': 'application/json',
    'Authorization': `Bearer ${localStorage.getItem('token')}`,
  }
});

let isRefreshing = false;
let refreshSubscribers = [];

const expireIn     = localStorage.getItem('expiredIn');
const refreshToken = localStorage.getItem('refreshToken');
const email        = localStorage.getItem('email');

instance.interceptors.response.use(response => {
  return response;
}, error => {
  const { config, status } = error;
  const originalRequest = config;

  if (status >= 400 && status < 500) {
    if (!isRefreshing) {
      isRefreshing = true;
      axios.post(`${CONFIG.baseURLAuth}user/refreshToken`, {
        refreshToken,
        email
      }).then(newToken => {
        console.log('Token refreshing..!');
        isRefreshing = false;
        onRrefreshed(newToken);
      }).catch(err => {
        console.log(err);
      });
    }
  }

  const retryOrigReq = new Promise((resolve, reject) => {
    subscribeTokenRefresh(token => {
      // replace the expired token and retry
      originalRequest.headers['Authorization'] = 'Bearer ' + token;
      resolve(axios(originalRequest));
    });
  });
  return retryOrigReq;

});

const subscribeTokenRefresh = (cb) => {
  console.log(cb);
  refreshSubscribers.push(cb);
}

const onRrefreshed = (token) => {
  refreshSubscribers.map(cb => {
    cb(token);
  });
}

const apiGet = (url, headers = {
  headers: headers
}, urlAuth = CONFIG.baseURL) => {
  store.dispatch({
    type: SHOW_LOADING
  });
  return new Promise((resolve, reject) => {
    instance.get(url, Object.assign({}, headers, {
      baseURL: urlAuth
    })).then(data => {
      store.dispatch({
        type: HIDE_LOADING
      });
      resolve(data);
    }, err => {
      store.dispatch({
        type: HIDE_LOADING
      });
      store.dispatch({
        type: SHOW_PROMPT,
        payload: {
          content: CONFIG.httpErrorContent
        }
      });
      reject(err);
    });
  });
}

const apiPost = (url, params = {}, headers = headers, urlAuth = CONFIG.baseURL) => {
  store.dispatch({
    type: SHOW_LOADING
  });
  return new Promise((resolve, reject) => {
    instance.post(url, params, Object.assign({}, headers, {
      baseURL: urlAuth
    })).then(data => {
      store.dispatch({
        type: HIDE_LOADING
      });
      resolve(data);
    }, err => {
      store.dispatch({
        type: HIDE_LOADING
      });
      store.dispatch({
        type: SHOW_PROMPT,
        payload: {
          content: CONFIG.httpErrorContent
        }
      });
      reject(err);
    });
  });
}

const apiDelete = (url, headers = {
  headers: getHeaders()
}, urlAuth = CONFIG.baseURL) => {
  store.dispatch({
    type: SHOW_LOADING
  });
  return new Promise((resolve, reject) => {
    urlAuth = urlAuth || CONFIG.baseURLAuth;
    instance.delete(url, Object.assign({}, headers, {
      baseURL: urlAuth
    })).then(data => {
      store.dispatch({
        type: HIDE_LOADING
      });
      resolve(data);
    }, err => {
      store.dispatch({
        type: HIDE_LOADING
      });
      store.dispatch({
        type: SHOW_PROMPT,
        payload: {
          content: CONFIG.httpErrorContent
        }
      });
      reject(err);
    });
  });
}

const apiPut = (url, params = {}, headers = {
  headers: getHeaders()
}, urlAuth = CONFIG.baseURL) => {
  store.dispatch({
    type: SHOW_LOADING
  });
  return new Promise((resolve, reject) => {
    urlAuth = urlAuth || CONFIG.baseURLAuth;
    instance.put(url, params, Object.assign({}, headers, {
      baseURL: urlAuth
    })).then(data => {
      store.dispatch({
        type: HIDE_LOADING
      });
      resolve(data);
    }, err => {
      store.dispatch({
        type: HIDE_LOADING
      });
      store.dispatch({
        type: SHOW_PROMPT,
        payload: {
          content: CONFIG.httpErrorContent
        }
      });
      reject(err);
    });
  });
}

export const http = {
  get: apiGet,
  post: apiPost,
  delete: apiDelete,
  put: apiPut
}
