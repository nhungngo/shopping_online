export const listWomanLinks = {
    nameCategory: 'Woman',
    links: [
      { name: 'Hi Tops', link: '/product_list/hi_top' },
      { name: 'Running Shoes', link: '/product_list/shoes' },
      { name: 'Jackets and Coats', link: '/product_list/jacket_coat' }
    ]
};
export const listManLinks = {
    nameCategory: 'Man',
      subCategory: [
        {
          subName: 'Footwear',
          links: [
            { name: 'Astro Trainers', link: '/product_list/astro_trainer' },
            { name: 'Basketball Shoes', link: '/product_list/basketball_shoes' },
            { name: 'Boots', link: '/product_list/boot' },
            { name: 'Canvas Shoes', link: '/product_list/canvas_shoes' },
            { name: 'Football Boots', link: '/product_list/football_boot' },
            { name: 'Golf Shoes', link: '/product_list/golf_shoes' },
            { name: 'Indoor and Court Trainers', link: '/product_list/indoor_court' }
          ]
        },
        {
          subName: 'Clothing',
          links: [
            { name: 'Base Layer', link: '/product_list/base_layer' },
            { name: 'Character', link: '/product_list/character' },
            { name: 'Chinos', link: '/product_list/chinos' },
            { name: 'Combats', link: '/product_list/combats' },
            { name: 'Cricket Clothing', link: '/product_list/cricket_clothing' },
            { name: 'Fleeces', link: '/product_list/fleeces' },
            { name: 'Gilets', link: '/product_list/gilets' },
            { name: 'Golf Tops', link: '/product_list/golf_top' }
          ]
        },
        {
          subName: 'Clearance',
          links: [
            { name: 'Jackets', link: '/product_list/jacket_coat' }
          ]
        }
      ]
};
export const listPageLinks = {
    nameCategory: 'Pages',
      links: [
        { name: 'Product List', link: '/product_list' },
        { name: 'Shopping Cart', link: '/cart' },
        { name: 'Checkout', link: '/checkout' },
        { name: 'About', link: '/about' },
        { name: 'My account', link: '/account' },
        { name: 'FAQ', link: '/faq' },
        { name: 'Privacy Policy', link: '/policy' },
        { name: 'Terms &amp; Conditions', link: '/terms_conditions' }
      ]
};