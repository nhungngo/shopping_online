import AccountContainer from './containers/account/account.container'
import HomeContainer from './containers/home/home.container'
import LoginContainer from './containers/login/login.container'
import HeaderContainer from './containers/header/header.container'
import { FooterContainer } from './containers/footer/footer.container'
import { AsideContainer } from './containers/aside/aside.container'
import cartContainer from './containers/carts/cart.container'
import ProductListContainer from './containers/productList/product-list.container'
import ProductDetail from './containers/productDetail/product-detail.container'
import contactContainer from './containers/contact/contact.container'
import CheckOut from './containers/checkOut/checkout.container'
import PrintBill from './containers/checkOut/printBill'
import RegisterContainer from './containers/register/register.container'
import AboutContainer from './containers/about/about.container'
import PolicyContainer from './containers/policy/policy.container'
import TermsConditionsContainer from './containers/terms-conditions/terms-conditions.container'
import PageNotFoundContainer from './containers/404/page-not-found.container'

export const routes = [
  {
    path: '/',
    exact: true,
    component: HomeContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/login',
    exact: true,
    component: LoginContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/register',
    exact: true,
    component: RegisterContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/about',
    exact: true,
    component: AboutContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/account',
    exact: true,
    component: AccountContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/cart',
    exact: true,
    component: cartContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/policy',
    exact: true,
    component: PolicyContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/terms_conditions',
    exact: true,
    component: TermsConditionsContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/kids',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/jacket_coat',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/shoes',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/astro_trainer',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/basketball_shoes',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/boot',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/canvas_shoes',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/football_boot',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/golf_shoes',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/indoor_court',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/base_layer',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/character',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/chinos',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/combats',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/cricket_clothing',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/fleeces',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/gilets',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/golf_top',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product_list/hi_top',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/product-detail/:id',
    exact: false,
    component: ProductDetail,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/contact',
    exact: true,
    component: contactContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/checkout',
    exact: true,
    component: CheckOut,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/printBill',
    exact: true,
    component: PrintBill,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '/search',
    exact: true,
    component: ProductListContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  },
  {
    path: '**',
    exact: true,
    component: PageNotFoundContainer,
    sidebar: AsideContainer,
    header: HeaderContainer,
    footer: FooterContainer,
    showSidebar: false,
    showHeader: true,
    showFooter: true
  }
]
