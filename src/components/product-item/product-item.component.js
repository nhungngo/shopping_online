import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Modal } from 'antd';
import { http } from '../../services/http.service';
import { connect } from 'react-redux';
import { GET_CART } from '../../actions/actions';
import PropTypes from 'prop-types';

const addCartBtnBaseStyle = {
  textButton: 'ADD TO CARD',
  styleButton: {
    color: '#a8aeb3',
    border: '1px #ededed solid',
    background: '#fff',
    disabled: null
  }
};
const addCartBtnAddedStyle = {
  textButton: 'ADDED',
  styleButton: {
    disabled: true,
    border: '1px #ededed solid',
    background: '#cc3304'
  }
};

class ProductItem extends Component {
  constructor(props) {
    super(props);
    this.state = {
      addCartBtnStyle: addCartBtnBaseStyle,
      imgHeight: null
    };
  }

  componentDidMount() {
    const { carts, inforItem } = this.props;
    carts.map(cart => {
      if (cart.id === inforItem.id) {
        this.setState({
          addCartBtnStyle: addCartBtnAddedStyle
        });
      }
      return this.state.addCartBtnStyle;
    });
  }
  componentDidUpdate(prevProps) {
    const { carts, inforItem } = this.props;
    if (carts !== prevProps.carts) {
      prevProps.carts.map(cart => {
        if (cart.id === inforItem.id) {
          this.setState({
            addCartBtnStyle: addCartBtnBaseStyle
          });
        }
        return this.state.addCartBtnStyle;
      });
      carts.map(cart => {
        if (cart.id === inforItem.id) {
          this.setState({
            addCartBtnStyle: addCartBtnAddedStyle
          });
        }
        return this.state.addCartBtnStyle;
      });
    }
  }
  // getSnapshotBeforeUpdate(prevProps, prevState) {
  //   http.get('carts').then(res => {
  //     for (var i in res.data) {
  //       if (res.data[i].id === this.props.inforItem.id) {
  //         return 1
  //       }
  //     }
  //   })
  //   return 0
  // }

  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   if (snapshot === 1) {
  //     this.setState({
  //       textButton: 'ADDED',
  //       styleButton: {
  //         disabled: true,
  //         border: '1px #ededed solid',
  //         background: '#cc3304'
  //       }
  //     })
  //   }
  //   if (snapshot === 0) {
  //     this.setState({
  //       textButton: 'ADD TO CARD',
  //       styleButton: {
  //         color: '#a8aeb3',
  //         border: '1px #ededed solid',
  //         background: '#fff',
  //         disabled: null
  //       },
  //       imgHeight: null
  //     })
  //   }
  // }

  // shouldComponentUpdate() {}

  showModal = i => {
    this.setState({
      visible: true,
      currentImage: i
    });
  };

  handleOk = e => {
    this.setState({
      visible: false
    });
  };

  handleCancel = e => {
    this.setState({
      visible: false
    });
  };

  addToCart = () => {
    this.setState({
      addCartBtnStyle: addCartBtnAddedStyle
    });
    this.props.inforItem.quantity = 1;
    http.post('carts', this.props.inforItem).then(() => {
      http.get('carts').then(res => {
        this.props.addToCart(res.data);
      });
    });
  };

  onImgLoad = ({ target: img }) => {
    let height = Number(img.offsetWidth) / 0.75 + 'px';
    this.setState({
      imgHeight: height
    });
  };

  render() {
    const { inforItem, styleWith } = this.props;
    const { imgHeight, visible, addCartBtnStyle } = this.state;
    let typeProduct = '';
    switch (inforItem.type) {
      case 'sale':
        typeProduct = <div className="sticker sticker-sale" />;
        break;
      case 'new':
        typeProduct = <div className="sticker sticker-new" />;
        break;
      default:
        typeProduct = '';
        break;
    }
    const imgWrapStyle = {
      width: styleWith,
      marginRight: '0px'
    };
    const imgStyle = {
      height: imgHeight
    };

    return (
      <div className="active" style={imgWrapStyle}>
        <div>
          <div className="product-item">
            <div className="pi-img-wrapper">
              <img
                onLoad={this.onImgLoad}
                src={`/assets/pages/img/products/${inforItem.nameImage}.jpg`}
                className="img-responsive"
                style={imgStyle}
                alt={inforItem.productName}
              />
              <div>
                <a
                  onClick={() => this.showModal(inforItem.nameImage)}
                  className="btn btn-default fancybox-button text-white"
                >
                  Zoom
                </a>
                <Modal
                  className="gallery-modal"
                  visible={visible}
                  onOk={this.handleOk}
                  onCancel={this.handleCancel}
                >
                  <img
                    src={`/assets/pages/img/products/${
                      inforItem.nameImage
                    }.jpg`}
                    alt=""
                    width="100%"
                  />
                </Modal>
                <Link
                  to={{
                    pathname: `/product-detail/${inforItem.id}`,
                    query: { id: inforItem.id }
                  }}
                  className="btn btn-default fancybox-fast-view"
                >
                  View
                </Link>
              </div>
            </div>
            <h3>
              <a href={`/product-detail/${inforItem.id}`}>
                {inforItem.productName}
              </a>
            </h3>
            <div className="pi-price">${inforItem.cost}</div>
            <button
              className="btn btn-default add2cart"
              style={addCartBtnStyle.styleButton}
              onClick={this.addToCart}
              disabled={addCartBtnStyle.styleButton.disabled}
            >
              {addCartBtnStyle.textButton}
            </button>
            {typeProduct}
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    carts: state.carts
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addToCart: response =>
      dispatch({ type: GET_CART, payload: { carts: response } })
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductItem);

ProductItem.propTypes = {
  inforItem: PropTypes.object,
  styleWith: PropTypes.string
};
ProductItem.defaultProps = {
  inforItem: {},
  styleWith: ''
};
