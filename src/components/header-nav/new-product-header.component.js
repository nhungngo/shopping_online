import React, { Component } from 'react';
import { http } from "../../services/http.service";
import { connect } from "react-redux";
import { GET_NEW_ITEM, GET_CART } from "../../actions/actions";
import NewItemComponent  from "./new-item.component";
class NewProductHeaderComponent  extends Component {

    componentDidMount = () => {
      this.getNewItem();
    };

    getNewItem() {
      http.get("products").then(res => {
        this.props.getNewItem(res.data);
      });
    }

    fillIndex = id => {
      let { newItems } = this.props;
      let result = -1;
      newItems.forEach((newItem, index) => {
        if (newItem.id === id) {
          result = index;
        }
      });
      return result;
    };

    addToCart = id => {
      let { newItems } = this.props;
      let index = this.fillIndex(id);
      if (index !== -1) {
          newItems[index].quantity = 1;
        http.post("carts", newItems[index]).then(res => {});
      }
      http.get("carts").then(res => {
        this.props.addToCart(res.data);
      });
    };

    render() {
      return (
        <div className="row">
          { this.props.newItems.filter(p => {return p.category ==='new-item'}).map((val, i) => (
            <NewItemComponent
              inforItem={val}
              key={i}
              addToCart={this.addToCart}
            />
          ))}
        </div>
      );
    }
}
const mapStateToProps = state => {
  return {
    newItems: state.newItems
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getNewItem: response =>
      dispatch({ type: GET_NEW_ITEM, payload: { newItems: response } }),
    addToCart: response =>
      dispatch({ type: GET_CART, payload: { carts: response } })
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(NewProductHeaderComponent);
