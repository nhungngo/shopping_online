import React, { Component } from "react"
import { Link } from "react-router-dom"

class NewItemComponent extends Component {
  constructor(props) {
    super(props)
    this.state = {
      textButton: "ADD TO CARD",
      styleButton: {
        color: "#a8aeb3",
        border: "1px #ededed solid"
      }
    }
  }
  addToCart = () => {
    this.setState({
      textButton: "ADDED",
      styleButton: {
        border: "1px #1aa71a solid",
        color: "#1aa71a"
      }
    })
    this.props.addToCart(this.props.inforItem.id)
  }
  render() {
    var { inforItem } = this.props
    return (
      <div className="col-md-3 col-sm-4 col-xs-6">
        <div className="product-item">
          <div className="pi-img-wrapper">
            <Link to={{ pathname: `/product-detail/${inforItem.id}` }}>
              <img
                className="w-100"
                src={`/assets/pages/img/products/${inforItem.nameImage}.jpg`}
                alt=''
              />
            </Link>
          </div>
          <h3>
            <Link to={{ pathname: `/product-detail/${inforItem.id}` }}>
              {inforItem.productName}
            </Link>
          </h3>
          <div className="pi-price">{inforItem.cost}</div>
          <button
            className="btn btn-default add2cart"
            style={this.state.styleButton}
            onClick={this.addToCart}
          >
            {this.state.textButton}
          </button>
        </div>
      </div>
    )
  }
}

export default NewItemComponent
