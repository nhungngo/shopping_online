import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import NewProductHeaderComponent from './new-product-header.component'
import './header-nav.scss'
import SearchComponent from '../search/search.component'
import { connect } from 'react-redux'
import * as action from './../../actions/actions'

class HeaderNavComponent extends Component {
  handleLink = () => {
    this.props.onSearch({
      search: ''
    })
  }
  render() {
    return (
      <div className="header-navigation">
        <ul>
          <li className="">
            <Link
              onClick={this.handleLink}
              //   className="dropdown dropdown-toggle"
              className="link-dropdown"
              data-toggle="dropdown"
              data-target="#"
              to="/"
            >
              Woman
            </Link>
            {/* <!-- BEGIN DROPDOWN MENU --> */}
            <ul className="dropdown-menu">
              <li className="dropdown-submenu">
                <Link onClick={this.handleLink} to="/product_list/hi_top">
                  Hi Tops <i className="fa fa-angle-right" />
                </Link>
                <ul className="dropdown-menu" role="menu">
                  <li>
                    <Link onClick={this.handleLink} to="">
                      Second Level Link
                    </Link>
                  </li>
                  <li>
                    <Link onClick={this.handleLink} to="">
                      Second Level Link
                    </Link>
                  </li>
                  <li className="dropdown-submenu">
                    <Link
                      onClick={this.handleLink}
                      className="dropdown-toggle"
                      data-toggle="dropdown"
                      data-target="#"
                      to="/"
                    >
                      Second Level Link
                      <i className="fa fa-angle-right" />
                    </Link>
                    <ul className="dropdown-menu">
                      <li>
                        <Link onClick={this.handleLink} to="">
                          Third Level Link
                        </Link>
                      </li>
                      <li>
                        <Link onClick={this.handleLink} to="">
                          Third Level Link
                        </Link>
                      </li>
                      <li>
                        <Link onClick={this.handleLink} to="">
                          Third Level Link
                        </Link>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              <li>
                <Link onClick={this.handleLink} to="/product_list/shoes">
                  Running Shoes
                </Link>
              </li>
              <li>
                <Link onClick={this.handleLink} to="/product_list/jacket_coat">
                  Jackets and Coats
                </Link>
              </li>
            </ul>
            {/* <!-- END DROPDOWN MENU --> */}
          </li>
          <li className=" dropdown-megamenu">
            <Link
              onClick={this.handleLink}
              //   className="dropdown dropdown-toggle"
              className="link-dropdown"
              data-toggle="dropdown"
              data-target="#"
              to="/"
            >
              Man
            </Link>
            <ul className="dropdown-menu">
              <li>
                <div className="header-navigation-content">
                  <div className="row">
                    <div className="col-md-4 header-navigation-col">
                      <h4>Footwear</h4>
                      <ul>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/astro_trainer"
                          >
                            Astro Trainers
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/basketball_shoes"
                          >
                            Basketball Shoes
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/boot"
                          >
                            Boots
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/canvas_shoes"
                          >
                            Canvas Shoes
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/football_boot"
                          >
                            Football Boots
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/golf_shoes"
                          >
                            Golf Shoes
                          </Link>
                        </li>
                        <li>
                          <Link onClick={this.handleLink} to="/product_list">
                            Hi Tops
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/indoor_court"
                          >
                            Indoor and Court Trainers
                          </Link>
                        </li>
                      </ul>
                    </div>
                    <div className="col-md-4 header-navigation-col">
                      <h4>Clothing</h4>
                      <ul>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/base_layer"
                          >
                            Base Layer
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/character"
                          >
                            Character
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/chinos"
                          >
                            Chinos
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/combats"
                          >
                            Combats
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/cricket_clothing"
                          >
                            Cricket Clothing
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/fleeces"
                          >
                            Fleeces
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/gilets"
                          >
                            Gilets
                          </Link>
                        </li>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/golf_top"
                          >
                            Golf Tops
                          </Link>
                        </li>
                      </ul>
                    </div>
                    <div className="col-md-4 header-navigation-col">
                      <h4>Accessories</h4>
                      <ul>
                        <li>
                          <Link onClick={this.handleLink} to="">
                            Belts
                          </Link>
                        </li>
                        <li>
                          <Link onClick={this.handleLink} to="">
                            Caps
                          </Link>
                        </li>
                        <li>
                          <Link onClick={this.handleLink} to="">
                            Gloves, Hats and Scarves
                          </Link>
                        </li>
                      </ul>

                      <h4>Clearance</h4>
                      <ul>
                        <li>
                          <Link
                            onClick={this.handleLink}
                            to="/product_list/jacket_coat"
                          >
                            Jackets
                          </Link>
                        </li>
                        <li>
                          <Link onClick={this.handleLink} to="">
                            Bottoms
                          </Link>
                        </li>
                      </ul>
                    </div>
                    <div className="col-md-12 nav-brands">
                      <ul>
                        <li>
                          <Link onClick={this.handleLink} to="">
                            <img
                              title="esprit"
                              alt="esprit"
                              src="/assets/pages/img/brands/esprit.jpg"
                            />
                          </Link>
                        </li>
                        <li>
                          <Link onClick={this.handleLink} to="">
                            <img
                              title="gap"
                              alt="gap"
                              src="/assets/pages/img/brands/gap.jpg"
                            />
                          </Link>
                        </li>
                        <li>
                          <Link onClick={this.handleLink} to="">
                            <img
                              title="next"
                              alt="next"
                              src="/assets/pages/img/brands/next.jpg"
                            />
                          </Link>
                        </li>
                        <li>
                          <Link onClick={this.handleLink} to="">
                            <img
                              title="puma"
                              alt="puma"
                              src="/assets/pages/img/brands/puma.jpg"
                            />
                          </Link>
                        </li>
                        <li>
                          <Link onClick={this.handleLink} to="">
                            <img
                              title="zara"
                              alt="zara"
                              src="/assets/pages/img/brands/zara.jpg"
                            />
                          </Link>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
          </li>
          <li>
            <Link onClick={this.handleLink} to="/product_list/kids">
              Kids
            </Link>
          </li>
          <li className=" dropdown100 nav-catalogue">
            <Link
              onClick={this.handleLink}
              //   className="dropdown dropdown-toggle"
              className="link-dropdown"
              data-toggle="dropdown"
              data-target="#"
              to="/"
            >
              New
            </Link>
            <ul className="dropdown-menu">
              <li>
                <div className="header-navigation-content">
                  <NewProductHeaderComponent />
                </div>
              </li>
            </ul>
          </li>
          <li className="">
            <Link
              onClick={this.handleLink}
              //   className="dropdown dropdown-toggle"
              className="link-dropdown"
              data-toggle="dropdown"
              data-target="#"
              to=""
            >
              Pages
            </Link>
            <ul className="dropdown-menu">
              <li className="active">
                <Link onClick={this.handleLink} to="/">
                  Home Default
                </Link>
              </li>
              <li>
                <Link onClick={this.handleLink} to="/product_list">
                  Product List
                </Link>
              </li>
              <li>
                <Link onClick={this.handleLink} to="/cart">
                  Shopping Cart
                </Link>
              </li>
              <li>
                <Link onClick={this.handleLink} to="/checkout">
                  Checkout
                </Link>
              </li>
              <li>
                <Link onClick={this.handleLink} to="/about">
                  About
                </Link>
              </li>
              <li>
                <Link onClick={this.handleLink} to="/contact">
                  Contacts
                </Link>
              </li>
              <li>
                <Link onClick={this.handleLink} to="/account">
                  My account
                </Link>
              </li>
              <li>
                <Link onClick={this.handleLink} to="/faq">
                  FAQ
                </Link>
              </li>
              <li>
                <Link onClick={this.handleLink} to="/policy">
                  Privacy Policy
                </Link>
              </li>
              <li>
                <Link onClick={this.handleLink} to="/terms_conditions">
                  Terms &amp; Conditions
                </Link>
              </li>
            </ul>
          </li>
          <li>
            <Link onClick={this.handleLink} to="/contact">
              Contacts
            </Link>
          </li>
          <li>
            <Link onClick={this.handleLink} to="/about">
              About Us
            </Link>
          </li>
          {/* <!-- BEGIN TOP SEARCH --> */}
          <li className="menu-search">
            <span className="sep" />
            <i className="fa fa-search search-btn" />
            <div className="search-box">
              <SearchComponent placeholder={'Search'} />
            </div>
          </li>
          {/* <!-- END TOP SEARCH --> */}
        </ul>
      </div>
    )
  }
}
// export default HeaderNavComponent
const mapDispatchToProps = (dispatch, props) => {
  return {
    onSearch: response => {
      dispatch({ type: action.SEARCH, payload: { search: response.search } })
    }
  }
}
const mapStateToProps = state => {
  return { isLogin: state.isLogin, searchKey: state.search }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderNavComponent)
