import React, { Component } from 'react'
import Slider from 'react-slick'
import PropTypes from 'prop-types'

const baseConfig = {
  dots: false,
  infinite: false,
  speed: 500,
  slidesToShow: 5,
  slidesToScroll: 5,
  initialSlide: 0,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 3
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 2,
        initialSlide: 2
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
}
export default class CarouselComponent extends Component {
  render() {
    let carouselConfig = this.props.config
    let config = {
      ...baseConfig,
      ...carouselConfig
    }
    return <Slider {...config}>{this.props.carouselContent}</Slider>
  }
}

CarouselComponent.propTypes = {
  carouselContent: PropTypes.array,
  config: PropTypes.object
}
CarouselComponent.defaultProps = {
  carouselContent: [],
  config: baseConfig
}
