import React, { Component } from 'react'
import { Link } from "react-router-dom"
import './our-contact.scss'

class OurContactComponent extends Component {
  render() {
    return (
      <div>
        <h2>Our Contacts</h2>
        <address>
          35, Lorem Lis Street, Park Ave
          <br />
          California, US
          <br />
          <abbr title="Phone">P:</abbr> 300 323 3456
          <br />
        </address>
        <address>
          <strong>Email</strong>
          <br />
          <Link to="mailto:info@metronic.com">info@metronic.com</Link>
          <br />
          <Link to="mailto:support@metronic.com">support@metronic.com</Link>
        </address>
        <ul className="social-icons margin-bottom-10">
          <li>
            <Link
              to=""
              data-original-title="facebook"
              className="facebook"></Link>
          </li>
          <li>
            <Link
              to=""
              data-original-title="github"
              className="github"></Link>
          </li>
          <li>
            <Link
              to=""
              data-original-title="Goole Plus"
              className="googleplus"></Link>
          </li>
          <li>
            <Link
              to=""
              data-original-title="linkedin"
              className="linkedin"></Link>
          </li>
          <li>
          <Link
            to=""
            data-original-title="rss"
            className="rss"></Link>
          </li>
        </ul>
      </div>
    )
  }
}

export default OurContactComponent
