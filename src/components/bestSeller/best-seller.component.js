import React, { Component } from 'react';
import './best-seller.component.scss';
import { http } from '../../services/http.service';
import { connect } from 'react-redux';
import { GET_BEST_SELLER } from '../../actions/actions';
import { Link } from 'react-router-dom';
import PropTypes from 'prop-types';

export class BestSellerComponent extends Component {
  componentDidMount = () => {
    this.getBestSellers();
  };

  getBestSellers() {
    http.get('products').then(res => {
      this.props.getBestSellers(
        res.data.filter(p => {
          return p.bestSeller;
        })
      );
    });
  }
  render() {
    let { bestSellerProducts } = this.props;
    return (
      <div
        className={
          bestSellerProducts.length === 0
            ? 'd-none'
            : 'best-seller col-md-3 col-sm-4'
        }
      >
        <div className="component-title">Best Sellers</div>
        {bestSellerProducts.slice(0, 3).map((val, i) => (
          <div className="best-seller__item" key={i}>
            <div className="row">
              <div className="col-md-3 col-sm-4 pr-0">
                <Link
                  to={{
                    pathname: `/product-detail/${val.id}`
                  }}
                >
                  <img
                    src={`/assets/pages/img/products/${val.nameImage}.jpg`}
                    className="best-seller__img"
                    alt={val.productName}
                  />
                </Link>
              </div>
              <div className="col-md-9 col-sm-8">
                <div className="best-seller__name">
                  <Link
                    to={{
                      pathname: `/product-detail/${val.id}`
                    }}
                  >
                    {val.productName}
                  </Link>
                </div>
                <div className="best-seller__cost pi-price">${val.cost}</div>
              </div>
            </div>
          </div>
        ))}
      </div>
    );
  }
}
const mapStateToProps = state => {
  return {
    bestSellerProducts: state.bestSellers
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getBestSellers: response =>
      dispatch({ type: GET_BEST_SELLER, payload: { bestSellers: response } })
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(BestSellerComponent);

BestSellerComponent.propTypes = {
  bestSellerProducts: PropTypes.array
};
BestSellerComponent.defaultProps = {
  bestSellerProducts: []
};
