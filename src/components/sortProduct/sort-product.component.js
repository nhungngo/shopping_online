import React, { Component } from 'react';
import './sort-product.component.scss';
import { connect } from 'react-redux';
import * as action from './../../actions/actions';
import PropTypes from 'prop-types';

class SortProductComponent extends Component {
  onSort = e => {
    this.props.onSort({
      sort: e.target.value
    });
  };
  onShow = e => {
    this.props.onShow({
      show: parseInt(e.target.value)
    });
  };
  render() {
    let { sort, show } = this.props.display;
    return (
      <div className="sort">
        <span className="sort__by">SORT BY</span>{' '}
        <select defaultValue={sort} onChange={this.onSort}>
          <option value="default">Default</option>
          <option value="ascName">Name (A - Z)</option>
          <option value="desName">Name (Z - A)</option>
          <option value="ascPrice">Price (Low > High)</option>
          <option value="desPrice">Price (High > Low)</option>
        </select>
        <span className="sort__show">SHOW</span>
        <select defaultValue={show} onChange={this.onShow}>
          <option value="3">3</option>
          <option value="6">6</option>
          <option value="9">9</option>
          <option value="12">12</option>
          <option value="15">15</option>
          <option value="30">30</option>
          <option value="45">45</option>
          <option value="60">60</option>
        </select>
      </div>
    );
  }
}
const mapStatesToProps = state => {
  return {
    display: state.sort
  };
};
const mapDispatchToProps = (dispatch, props) => {
  return {
    onSort: response => {
      dispatch({ type: action.SORT, payload: { sort: response.sort } });
    },
    onShow: response => {
      dispatch({ type: action.SHOW, payload: { show: response.show } });
    }
  };
};
export default connect(
  mapStatesToProps,
  mapDispatchToProps
)(SortProductComponent);

SortProductComponent.propTypes = {
  display: PropTypes.shape({
    sort: PropTypes.string,
    show: PropTypes.number
  })
};
SortProductComponent.defaultProps = {
  display: {
    sort: 'default',
    show: 9
  }
};
