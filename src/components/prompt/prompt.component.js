import React, { Component } from 'react';
import { connect } from "react-redux";
import './prompt.component.scss';
import { HIDE_PROMPT } from "../../actions/actions";
import { CONFIG } from "../../services/config.service";

class PromptComponent extends Component {

  render() {
    const {show, title, content} = this.props.prompt;
    const titleView = title ? (<h5>{ title }</h5>) : null
    const conentView = content ? (<h2>{ content }</h2>) : null
    
    return (
      <div className={`prompt-container ${show ? "" : "hidden"}`}>
        <div className="prompt">
          { titleView }
          { conentView }
          <div className="btn-confirm">
            <button onClick={this.props.hidePromt}>Close</button>
          </div>
        </div>
      </div>
    )
  }

}

const mapStatesToProps = state => {
  return {
    prompt: state.prompt
  };
};

const mapDispatchToProps = dispatch => {
  return {
    hidePromt: (res) => {
      dispatch({ type: HIDE_PROMPT, payload: { title: CONFIG.httpErrorTitle, content: CONFIG.httpErrorContent } })
    }
  }
}

export default connect(mapStatesToProps, mapDispatchToProps)(PromptComponent);
