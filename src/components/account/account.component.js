import React, { Component } from 'react';
import { Form, Input, Select, Button, Checkbox, DatePicker } from 'antd';
import { connect } from 'react-redux';
import { http } from '../../services/http.service';
import moment from 'moment';
import './account.component.scss';
import { Radio } from 'antd';
import { Redirect } from 'react-router-dom';

const { Option } = Select;
const RadioGroup = Radio.Group;
class AccountComponent extends Component {
  state = {
    confirmDirty: false,
    confirmDirtyN: false,
    autoCompleteResult: [],
    formLayout: 'vertical',
    isCheckAgree: null,
    user: {}
  };
  componentWillMount() {
    if (localStorage) {
      let user = JSON.parse(localStorage.getItem('userInfor'));
      this.setState({
        user
      });
    }
  }
  componentDidMount() {
    let { user } = this.state;
    if (user !== {}) {
      this.props.form.setFieldsValue({
        address1: user.address1,
        address2: user.address2,
        city: user.city,
        company: user.company,
        country: user.country,
        email: user.email,
        firstname: user.firstname,
        gender: user.gender,
        lastname: user.lastname,
        phone: user.phone,
        postcode: user.postcode,
        regionstate: user.regionstate,
        birthday: moment(user.birthday)
      });
      console.log(user);
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    let agreement = this.props.form.getFieldValue('agreement');
    if (!agreement) {
      this.setState({
        isCheckAgree: false
      });
    }
    this.props.form.validateFieldsAndScroll((err, values) => {
      values.birthday = values.birthday._i;
      if (!err && agreement) {
        console.log(values);
        http.put(`https://nestapisd.herokuapp.com/user/updateProfile/`, values);
        alert('Success');
      }
    });
  };

  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({
      confirmDirty: this.state.confirmDirty || !!value
    });
  };

  handleConfirmBlurN = e => {
    const value = e.target.value;
    this.setState({
      confirmDirtyN: this.state.confirmDirtyN || !!value
    });
  };
  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };
  compareToFirstPasswordN = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('newPassword')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };
  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirm'], { force: true });
    }
    callback();
  };
  validateToNextPasswordN = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirtyN) {
      form.validateFields(['confirmPassword'], { force: true });
    }
    callback();
  };
  handleCheckAgree = () => {
    let agreement = this.props.form.getFieldValue('agreement');
    if (!agreement) {
      this.setState({
        isCheckAgree: true
      });
    } else {
      this.setState({
        isCheckAgree: false
      });
    }
  };
  disabledDate = current => {
    return current > moment().endOf('day');
  };
  render() {
    if (!this.props.isLogin) {
      return <Redirect to="/login" />;
    }
    const { getFieldDecorator } = this.props.form;
    const { formLayout } = this.state;
    const formItemLayout =
      formLayout === 'horizontal'
        ? {
            labelCol: { span: 4 },
            wrapperCol: { span: 14 }
          }
        : null;

    const regexPhone = /(09([0-9]{8})|((03[2-9]|07(0|6|7|8|9)|08[1-5])([0-9]{7}))|(05((8|6)[2-9]|9(2|3|8|9))([0-9]{6})))+\b/;
    const regexPassword = /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/;
    const regexEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    const dateFormat = 'YYYY-MM-DD';

    return (
      <Form {...formItemLayout} onSubmit={this.handleSubmit}>
        <h2>My Account</h2>
        <div className="row">
          <div className="col">
            <Form.Item label="E-mail" hasFeedback>
              {getFieldDecorator('email', {
                //
                rules: [
                  {
                    pattern: regexEmail,
                    message: 'The input is not a valid E-mail!'
                  },
                  {
                    required: true,
                    message: 'Please input your E-mail!'
                  },
                  {
                    validator: this.validateExistEmail
                  }
                ]
              })(<Input />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Form.Item label="Password" hasFeedback>
              {getFieldDecorator('password', {
                rules: [
                  {
                    required: true,
                    message: 'Please input your password!'
                  },
                  {
                    validator: this.validateToNextPassword
                  },
                  {
                    pattern: regexPassword,
                    message:
                      'Password has at least one upper case, lower case, digit, special character and minimum eight in length'
                  }
                ]
              })(<Input type="password" />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Form.Item label="New Password" hasFeedback>
              {getFieldDecorator('newPassword', {
                rules: [
                  {
                    required: true,
                    message: 'Please input your password!'
                  },
                  {
                    validator: this.validateToNextPasswordN
                  },
                  {
                    pattern: regexPassword,
                    message:
                      'Password has at least one upper case, lower case, digit, special character and minimum eight in length'
                  }
                ]
              })(<Input type="password" />)}
            </Form.Item>
          </div>
          <div className="col-md-6">
            <Form.Item label="Confirm Password" hasFeedback>
              {getFieldDecorator('confirmPassword', {
                rules: [
                  {
                    required: true,
                    message: 'Please confirm your new password!'
                  },
                  {
                    validator: this.compareToFirstPasswordN
                  }
                ]
              })(<Input type="password" onBlur={this.handleConfirmBlurN} />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Form.Item label="First name" hasFeedback>
              {getFieldDecorator('firstname', {
                rules: [
                  {
                    required: true,
                    message: 'Please input your first name!',
                    whitespace: true
                  }
                ]
              })(<Input />)}
            </Form.Item>
          </div>
          <div className="col-md-6">
            <Form.Item label="Last name" hasFeedback>
              {getFieldDecorator('lastname', {
                rules: [
                  {
                    required: true,
                    message: 'Please input your last name!',
                    whitespace: true
                  }
                ]
              })(<Input />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Form.Item label="Birth day" hasFeedback>
              {getFieldDecorator('birthday', {
                rules: [
                  {
                    required: true,
                    message: 'Please input your phone birthday!'
                  }
                ]
              })(
                <DatePicker
                  disabledDate={this.disabledDate}
                  format={dateFormat}
                />
              )}
            </Form.Item>
          </div>

          <div className="col-md-6">
            <Form.Item label="Phone Number" hasFeedback>
              {getFieldDecorator('phone', {
                rules: [
                  {
                    required: true,
                    message: 'Please input your phone number!'
                  },
                  {
                    pattern: regexPhone,
                    message: 'Please input an valid phone'
                  }
                ]
              })(<Input style={{ width: '100%' }} />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <Form.Item label="Gender" hasFeedback>
              {getFieldDecorator('gender', {
                rules: [
                  { required: true, message: 'Please choose your gender!' }
                ]
              })(
                <RadioGroup>
                  <Radio value="male">Male</Radio>
                  <Radio value="female">Female</Radio>
                </RadioGroup>
              )}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <Form.Item label="Company" hasFeedback>
              {getFieldDecorator('company', {
                rules: [{ required: false, message: '' }]
              })(<Input style={{ width: '100%' }} />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Form.Item label="Address1" hasFeedback>
              {getFieldDecorator('address1', {
                rules: [
                  { required: true, message: 'Please input your address' }
                ]
              })(<Input style={{ width: '100%' }} />)}
            </Form.Item>
          </div>
          <div className="col-md-6">
            <Form.Item label="Address2" hasFeedback>
              {getFieldDecorator('address2', {
                rules: [{ required: false, message: '' }]
              })(<Input style={{ width: '100%' }} />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Form.Item label="City" hasFeedback>
              {getFieldDecorator('city', {
                rules: [{ required: true, message: 'Please input your city' }]
              })(<Input style={{ width: '100%' }} />)}
            </Form.Item>
          </div>
          <div className="col-md-6">
            <Form.Item label="Post Code" hasFeedback>
              {getFieldDecorator('postcode', {
                rules: [
                  { required: true, message: 'Please input your post code' }
                ]
              })(<Input style={{ width: '100%' }} />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Form.Item label="Country" hasFeedback>
              {getFieldDecorator('country', {
                rules: [
                  { required: true, message: 'Please choose your country' }
                ]
              })(
                <Select
                  style={{ width: '100%' }}
                  placeholder="--Please Select--"
                >
                  <Option value="Vietnam">Vietnam</Option>
                  <Option value="USA">USA</Option>
                  <Option value="Jajan">Jajan</Option>
                  <Option value="Korea">Korea</Option>
                  <Option value="China">China</Option>
                </Select>
              )}
            </Form.Item>
          </div>
          <div className="col-md-6">
            <Form.Item label="Region/State" hasFeedback>
              {getFieldDecorator('regionstate', {
                rules: [
                  {
                    required: true,
                    message: 'Please select your region/state'
                  }
                ]
              })(
                <Select
                  style={{ width: '100%' }}
                  placeholder="--Please Select--"
                >
                  <Option value="Vietnam">Vietnam</Option>
                  <Option value="USA">USA</Option>
                  <Option value="Jajan">Jajan</Option>
                  <Option value="Korea">Korea</Option>
                  <Option value="China">China</Option>
                </Select>
              )}
            </Form.Item>
          </div>
        </div>
        <Form.Item>
          {getFieldDecorator('agreement', {})(
            <Checkbox
              onClick={this.handleCheckAgree}
              className={
                this.state.isCheckAgree !== false ? 'checked' : 'error-msg '
              }
            >
              I have read the <a href="/agreement">agreement</a>
            </Checkbox>
          )}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Lưu lại
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLogin: state.isLogin
  };
};
const mapDispatchToProps = dispatch => {
  return {};
};

export const WrappedAccountForm = Form.create({ name: 'myAccount' })(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(AccountComponent)
);
