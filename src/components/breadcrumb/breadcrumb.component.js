import React, { Component } from 'react'
import './breadcrumb.component.scss'
import { Breadcrumb } from 'antd'
import { Link } from 'react-router-dom'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { SEARCH } from '../../actions/actions'

const baseConfig = [
  {
    path: '/',
    breadcrumbName: 'home'
  }
]
function handleLink() {
  this.props.onSearch({
    search: ''
  })
}
function itemRender(route, params, routes, paths) {
  const last = routes.indexOf(route) === routes.length - 1
  return last ? (
    <span>{route.breadcrumbName}</span>
  ) : (
    <Link to={route.path} onClick={handleLink}>
      {route.breadcrumbName}
    </Link>
  )
}
class BreadcrumbComponent extends Component {
  render() {
    let { routesConfig } = this.props
    let config = [...baseConfig, ...routesConfig]

    return (
      <div className="breadcrumb">
        <Breadcrumb itemRender={itemRender} routes={config} separator={'>'} />
      </div>
    )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onSearch: response => {
      dispatch({ type: SEARCH, payload: { search: response.search } })
    }
  }
}
export default connect(mapDispatchToProps)(BreadcrumbComponent)

BreadcrumbComponent.propTypes = {
  config: PropTypes.array,
  routesConfig: PropTypes.array
}
BreadcrumbComponent.defaultProps = {
  config: baseConfig,
  routesConfig: []
}
