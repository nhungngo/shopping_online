import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import * as Constants from '../../utils/constant';
export class VerticalCategory extends Component {
  constructor(props) {
    super(props)
    let actiiveCategory = '';
    let actiiveSubCategory = '';
    const url = window.location.href;
    Constants.listPageLinks.links.forEach(element => {
      if (url.includes(element.link)) {
        actiiveCategory = Constants.listPageLinks.nameCategory;
      }
    });
    Constants.listWomanLinks.links.forEach(element => {
      if (url.includes(element.link)) {
        actiiveCategory = Constants.listWomanLinks.nameCategory;
      }
    });
    Constants.listManLinks.subCategory.forEach(element => {
      element.links.forEach(subElement => {
        if (url.includes(subElement.link)) {
          actiiveCategory = Constants.listManLinks.nameCategory;
          actiiveSubCategory = element.subName;
        }
      });
    });
    this.state = {
      activeLink: actiiveCategory,
      subActiveLink: actiiveSubCategory
    };
    
  }

  handleLink(activeLink) {
    if (activeLink === this.state.activeLink) {
      this.setState({ activeLink: '', subActiveLink: '' });
    } else {
      this.setState({ activeLink });
    }
  }
  handleSubLink(subActiveLink) {
    if (subActiveLink === this.state.subActiveLink) {
      this.setState({ subActiveLink: '' });
    } else {
      this.setState({ subActiveLink });
    }
  }

  render() {
    const activeClass = {
      color: 'red'
    };
    return (
      <div className="sidebar col-md-3 col-sm-4" >
        <ul className="list-group margin-bottom-25 sidebar-menu">
          <li className="list-group-item clearfix dropdown">
            <span onClick={(e) => this.handleLink(Constants.listWomanLinks.nameCategory)} className={this.state.activeLink === Constants.listWomanLinks.nameCategory ? 'collapsed' : ''} style={{ cursor: 'pointer' }}>
              <i className="fa fa-angle-right"></i> {Constants.listWomanLinks.nameCategory}
            </span>
            <ul className="dropdown-menu" style={{ display: this.state.activeLink === Constants.listWomanLinks.nameCategory ? 'block' : 'none' }}>
              {
                Constants.listWomanLinks.links.map((item, i) => (
                  <li key={i}><NavLink activeStyle={activeClass} to={item.link}><i className="fa fa-angle-right"></i> {item.name}</NavLink></li>
                ))
              }
            </ul>
          </li>
          <li className="list-group-item clearfix dropdown">
            <span onClick={(e) => this.handleLink(Constants.listManLinks.nameCategory)} className={this.state.activeLink === Constants.listManLinks.nameCategory ? 'collapsed' : ''} style={{ cursor: 'pointer' }}>
              <i className="fa fa-angle-right"></i>
              {Constants.listManLinks.nameCategory}
            </span>
            <ul className="dropdown-menu" style={{ display: this.state.activeLink === Constants.listManLinks.nameCategory ? 'block' : 'none' }}>
              {
                Constants.listManLinks.subCategory.map((item, i) => (
                  <li className="list-group-item dropdown clearfix" key={i}>
                    <span onClick={(e) => this.handleSubLink(item.subName)} className={this.state.activeLink === Constants.listManLinks.nameCategory ? 'collapsed' : ''} style={{ cursor: 'pointer' }}><i className="fa fa-angle-right"></i> {item.subName} </span>
                    <ul className="dropdown-menu" style={{ display: this.state.subActiveLink === item.subName ? 'block' : 'none' }}>
                      {
                        item.links.map((item2, i2) => (
                          <li className="list-group-item dropdown clearfix" key={i2}>
                            <NavLink activeStyle={activeClass} to={item2.link}><i className="fa fa-angle-right"></i> {item2.name} </NavLink>
                          </li>
                        ))
                      }
                    </ul>
                  </li>
                ))
              }
            </ul>
          </li>
          <li className="list-group-item clearfix"><NavLink activeStyle={activeClass} to="/product_list/kids"><i className="fa fa-angle-right"></i> Kids</NavLink></li>
          <li className="list-group-item clearfix dropdown">
            <span onClick={(e) => this.handleLink(Constants.listPageLinks.nameCategory)} className={this.state.activeLink === Constants.listPageLinks.nameCategory ? 'collapsed' : ''} style={{ cursor: 'pointer' }}>
              <i className="fa fa-angle-right"></i> {Constants.listPageLinks.nameCategory}
            </span>
            <ul className="dropdown-menu" style={{ display: this.state.activeLink === Constants.listPageLinks.nameCategory ? 'block' : 'none' }}>
              {
                Constants.listPageLinks.links.map((item, i) => (
                  <li key={i}><NavLink exact activeStyle={activeClass} to={item.link}><i className="fa fa-angle-right"></i> {item.name}</NavLink></li>
                ))
              }
            </ul>
          </li>
          <li className="list-group-item clearfix"><NavLink activeStyle={activeClass} to="/contact"><i className="fa fa-angle-right"></i> Contacts</NavLink></li>
          <li className="list-group-item clearfix"><NavLink activeStyle={activeClass} to="/about"><i className="fa fa-angle-right"></i> About Us</NavLink></li>
        </ul>
      </div >
    );
  }
}
