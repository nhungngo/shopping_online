import React, { Component } from 'react'
import { connect } from 'react-redux'
import './search.component.scss'
import * as action from './../../actions/actions'
import PropTypes from 'prop-types'

class SearchComponent extends Component {
  constructor(props) {
    super(props)
    this.state = { value: '' }
  }
  handleChange = e => {
    this.setState({
      value: e.target.value
    })
  }
  onSearch = e => {
    e.preventDefault()
    this.props.onSearch({
      search: this.state.value
    })
    this.setState({
      value: ''
    })
  }
  render() {
    let { placeholder } = this.props
    return (
      <form onSubmit={this.onSearch} className="search-form">
        <div className="input-group">
          <input
            type="text"
            placeholder={placeholder}
            className="form-control"
            onChange={this.handleChange}
            value={this.state.value}
          />
          <button
            className="btn btn-primary"
            type="submit"
            style={{ fontSize: '13px' }}
            disabled={!this.state.value}
          >
            Search
          </button>
        </div>
      </form>
    )
  }
}
const mapStatesToProps = state => {
  return {
    searchKey: state.search
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    onSearch: response => {
      dispatch({ type: action.SEARCH, payload: { search: response.search } })
    }
  }
}
export default connect(
  mapStatesToProps,
  mapDispatchToProps
)(SearchComponent)

SearchComponent.propTypes = {
  searchKey: PropTypes.string,
  placeholder: PropTypes.string
}
SearchComponent.defaultProps = {
  searchKey: '',
  placeholder: ''
}
