import React, { Component } from 'react'
import { http } from '../../services/http.service'
import { GET_NEW_POPULAR, GET_CART } from '../../actions/actions'
import { connect } from 'react-redux'
import ProductItem from '../../components/product-item/product-item.component'

class PopularCart extends Component {
  componentWillMount() {
    this.getNewPopular()
  }
  getNewPopular = () => {
    http.get('products').then(res => {
      this.props.getNewPopular(res.data)
    })
  }
  render() {
    return (
      <div className="mt-5">
        <h5>MOST POPULAR PRODUCTS</h5>
        <div className="row margin-bottom-40">
          <div className="col-md-12 sale-product">
            <div className="owl-carousel owl-carousel5">
              {this.props.newPopular
                .filter(p => {
                  return p.category === 'popular-product'
                })
                .map((val, i) => (
                  <div className="col-md-3 col-sm-3 col-xs-3" key={i}>
                    <ProductItem inforItem={val} styleWith={'100%'} />
                  </div>
                ))}
            </div>
          </div>
        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    newPopular: state.newPopular
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getNewPopular: response =>
      dispatch({ type: GET_NEW_POPULAR, payload: { newPopular: response } }),
    addToCart: response =>
      dispatch({ type: GET_CART, payload: { carts: response } })
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PopularCart)
