import React, { Component } from 'react';
import 'antd/dist/antd.css';
import { LOG_IN } from '../../actions/actions';
import { connect } from 'react-redux';
import { Form, Icon, Input, Button, Checkbox } from 'antd';
import { Link } from 'react-router-dom';
import { Authentication } from '../../services/authen.service';
import jwt_decode from 'jwt-decode';

class LoginComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      errorAccount: '',
      isValid: false
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    this.props.form.validateFields((err, data) => {
      if (!err) {
        const user = {
          email: data.userName,
          password: data.password
        };
        Authentication.login(user).then(
          res => {
            Authentication.storeAuth(
              new Date().getTime() + 15 * 1000,
              res.data.token,
              res.data.refreshToken,
              user.email
            );
            let decoded = jwt_decode(res.data.token);
            localStorage.setItem('userInfor', JSON.stringify(decoded));
            console.log(decoded);

            console.log(localStorage.getItem('userInfor'));

            this.props.login();
          },
          err => {
            this.setState({
              errorAccount: 'Invalid email or password, please try again.'
            });
          }
        );
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;
    let isErr = false;
    if (this.state.errorAccount !== '') {
      isErr = true;
    }

    return (
      <div>
        <div className={isErr ? 'error-account' : 'd-none'}>
          {this.state.errorAccount}
        </div>
        <Form onSubmit={this.handleSubmit} className="login-form">
          <Form.Item hasFeedback>
            {getFieldDecorator('userName', {
              rules: [
                {
                  required: true,
                  message: 'Please input your Email!'
                },
                {
                  type: 'email',
                  message: 'Please input an valid email!'
                }
              ]
            })(
              <Input
                prefix={
                  <Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />
                }
                placeholder="Email"
              />
            )}
          </Form.Item>
          <Form.Item hasFeedback>
            {getFieldDecorator('password', {
              rules: [
                {
                  required: true,
                  message: 'Please input your Password!'
                }
              ]
            })(
              <Input
                prefix={
                  <Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />
                }
                type="password"
                placeholder="Password"
              />
            )}
          </Form.Item>
          <Form.Item>
            <div className="disable">
              {getFieldDecorator('remember', {
                valuePropName: 'checked',
                initialValue: true
              })(<Checkbox>Remember me</Checkbox>)}
              <Link className="login-form-forgot" to="/forgot_password">
                Forgot password
              </Link>
            </div>
            <Button
              type="primary"
              htmlType="submit"
              className="btn login-form-button"
            >
              Log in
            </Button>
            <div className="disable">
              Or <Link to="/register">register now!</Link>
            </div>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    login: () => {
      dispatch({ type: LOG_IN });
    }
  };
};

export const WrappedLoginForm = Form.create({ name: 'login' })(
  connect(
    null,
    mapDispatchToProps
  )(LoginComponent)
);
