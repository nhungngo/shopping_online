import React, { Component } from 'react'
import Slider from 'react-slick'
import './main-carousel.component.scss'
import { Link } from 'react-router-dom'

export default class MainCarouselComponent extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    }
    return (
      <div className="main-carousel ">
        <Slider
          ref={slider => (this.slider = slider)}
          {...settings}
          className="carousel slide carousel-slider"
        >
          <div className="main-carousel__item">
            <div className="item carousel-item-four">
              <div className="container">
                <div className="carousel-position-four text-center">
                  <h2 className="margin-bottom-20 carousel-title-v3 border-bottom-title text-uppercase animate fade-in-down">
                    Tones of <br />
                    <span className="color-red-v2">Shop UI Features</span>
                    <br /> designed
                  </h2>
                  <p className="carousel-subtitle-v2 animate fade-in-up">
                    Lorem ipsum dolor sit amet constectetuer diam <br />
                    adipiscing elit euismod ut laoreet dolore.
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="main-carousel__item">
            <div className="item carousel-item-five">
              <div className="container">
                <div className="carousel-position-four text-center">
                  <h2 className="carousel-title-v4 animate fade-in-down ">
                    Unlimted
                  </h2>
                  <p className="carousel-subtitle-v2 animate fade-in-down">
                    Layout Options
                  </p>
                  <p className="carousel-subtitle-v3 margin-bottom-30 animate fade-in-up">
                    Fully Responsive
                  </p>
                  <Link className="carousel-btn animate fade-in-up" to="#">
                    See More Details
                  </Link>
                  <img
                    className="carousel-position-five hidden-sm hidden-xs animate zoom-in"
                    src="assets/pages/img/shop-slider/slide2/price.png"
                    alt="Price"
                  />
                </div>
              </div>
            </div>
          </div>
          <div className="main-carousel__item">
            <div className="item carousel-item-six">
              <div className="container">
                <div className="carousel-position-four text-center">
                  <span className="carousel-subtitle-v3 margin-bottom-15 animate fade-in-down">
                    Full Admin &amp; Frontend
                  </span>
                  <p className="carousel-subtitle-v4 animate fade-in-down">
                    eCommerce UI
                  </p>
                  <p className="carousel-subtitle-v3 animate fade-in-down">
                    Is Ready For Your Project
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="main-carousel__item">
            <div className="item carousel-item-seven">
              <div className="">
                <h2 className="carousel-title-v1 margin-bottom-20 animate fade-in-down">
                  The most <br />
                  wanted bijouterie
                </h2>
                <div className="carousel-item-seven__btn">
                  <Link className="carousel-btn animate fade-in-up" to="#">
                    But It Now!
                  </Link>
                </div>
              </div>
            </div>
          </div>
        </Slider>
      </div>
    )
  }
}
