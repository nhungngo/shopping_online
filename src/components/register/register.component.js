import React, { Component } from 'react';
import { Form, Input, Select, Checkbox, Button, DatePicker } from 'antd';
import { GET_ACCOUNT, REGISTER, LOG_IN } from '../../actions/actions';
import { connect } from 'react-redux';
import moment from 'moment';
import { Radio } from 'antd';
import { Authentication } from '../../services/authen.service';
import { http } from '../../services/http.service';

const RadioGroup = Radio.Group;

const { Option } = Select;

class RegistrationForm extends Component {
  state = {
    confirmDirty: false,
    autoCompleteResult: [],
    formLayout: 'vertical',
    isCheckAgree: null,
    accounts: []
  };

  handleSubmit = e => {
    e.preventDefault();
    let agreement = this.props.form.getFieldValue('agreement');
    if (!agreement) {
      this.setState({
        isCheckAgree: false
      });
    }
    this.props.form.validateFieldsAndScroll((err, data) => {
      if (!err && agreement) {
        const user = {
          ...data,

          birthday: moment(new Date(data.birthday._d)).format('YYYY-MM-DD')
        };
        Authentication.register(user).then(
          res => {
            this.props.register();
          },
          err => {
            alert('The user is already exist, please change another email.');
          }
        );
      }
    });
    // console.log(agreement, this.state.isCheckAgree);
  };

  handleConfirmBlur = e => {
    const value = e.target.value;
    this.setState({ confirmDirty: this.state.confirmDirty || !!value });
  };

  compareToFirstPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && value !== form.getFieldValue('password')) {
      callback('Two passwords that you enter is inconsistent!');
    } else {
      callback();
    }
  };

  validateToNextPassword = (rule, value, callback) => {
    const form = this.props.form;
    if (value && this.state.confirmDirty) {
      form.validateFields(['confirmpassword'], { force: true });
    }
    callback();
  };
  validateExistEmail = (rule, value, callback) => {
    const form = this.props.form;

    http.get('accounts').then(res => this.setState({ accounts: res.data }));
    let { accounts } = this.state;
    let email = form.getFieldValue('email');
    console.log('accounts', accounts)
    let isExistEmail = accounts.filter(a => {
      return a.email === email;
    });
    if (isExistEmail.length > 0) {
      callback('Email is exist, please use other email or login');
    } else {
      callback();
    }
  };
  handleCheckAgree = () => {
    let agreement = this.props.form.getFieldValue('agreement');
    if (!agreement) {
      this.setState({
        isCheckAgree: true
      });
    } else {
      this.setState({
        isCheckAgree: false
      });
    }
    console.log('change', agreement, this.state.isCheckAgree);
  };
  disabledDate = current => {
    return current > moment().endOf('day');
  };
  render() {
    const { getFieldDecorator } = this.props.form;
    const { formLayout } = this.state;
    const formItemLayout =
      formLayout === 'horizontal'
        ? {
          labelCol: { span: 4 },
          wrapperCol: { span: 14 }
        }
        : null;

    const config = {
      rules: [
        { type: 'object', required: true, message: 'Please select one day!' }
      ]
    };
    const regexPhone = /(09([0-9]{8})|((03[2-9]|07(0|6|7|8|9)|08[1-5])([0-9]{7}))|(05((8|6)[2-9]|9(2|3|8|9))([0-9]{6})))+\b/;
    const regexPassword = /^(?=.*?[A-Z])(?=(.*[a-z]){1,})(?=(.*[\d]){1,})(?=(.*[\W]){1,})(?!.*\s).{8,}$/;
    const regexEmail = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
    const dateFormat = 'YYYY-MM-DD';
    return (
      <Form {...formItemLayout} onSubmit={this.handleSubmit}>
        <div className="row">
          <div className="col">
            <Form.Item label="E-mail" hasFeedback>
              {getFieldDecorator('email', {
                rules: [
                  {
                    pattern: regexEmail,
                    message: 'The input is not a valid E-mail!'
                  },
                  {
                    required: true,
                    message: 'Please input your E-mail!'
                  },
                  {
                    validator: this.validateExistEmail
                  }
                ]
              })(<Input />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Form.Item label="Password" hasFeedback>
              {getFieldDecorator('password', {
                rules: [
                  {
                    required: true,
                    message: 'Please input your password!'
                  },
                  {
                    validator: this.validateToNextPassword
                  },
                  {
                    pattern: regexPassword,
                    message:
                      'Password has at least one upper case, lower case, digit, special character and minimum eight in length'
                  }
                ]
              })(<Input type="password" />)}
            </Form.Item>
          </div>
          <div className="col-md-6">
            <Form.Item label="Confirm Password" hasFeedback>
              {getFieldDecorator('confirmpassword', {
                rules: [
                  {
                    required: true,
                    message: 'Please confirm your password!'
                  },
                  {
                    validator: this.compareToFirstPassword
                  }
                ]
              })(<Input type="password" onBlur={this.handleConfirmBlur} />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Form.Item label="First name" hasFeedback>
              {getFieldDecorator('firstname', {
                rules: [
                  {
                    required: true,
                    message: 'Please input your first name!',
                    whitespace: true
                  }
                ]
              })(<Input />)}
            </Form.Item>
          </div>
          <div className="col-md-6">
            <Form.Item label="Last name" hasFeedback>
              {getFieldDecorator('lastname', {
                rules: [
                  {
                    required: true,
                    message: 'Please input your last name!',
                    whitespace: true
                  }
                ]
              })(<Input />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Form.Item label="Birth day" hasFeedback>
              {getFieldDecorator('birthday', config)(
                <DatePicker
                  disabledDate={this.disabledDate}
                  format={dateFormat}
                />
              )}
            </Form.Item>
          </div>

          <div className="col-md-6">
            <Form.Item label="Phone Number" hasFeedback>
              {getFieldDecorator('phone', {
                rules: [
                  {
                    required: true,
                    message: 'Please input your phone number!'
                  },
                  {
                    pattern: regexPhone,
                    message: 'Please input an valid phone'
                  }
                ]
              })(<Input style={{ width: '100%' }} />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <Form.Item label="Gender" hasFeedback>
              {getFieldDecorator('gender', {
                rules: [
                  { required: true, message: 'Please choose your gender!' }
                ]
              })(
                <RadioGroup>
                  <Radio value="male">Male</Radio>
                  <Radio value="female">Female</Radio>
                </RadioGroup>
              )}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col">
            <Form.Item label="Company" hasFeedback>
              {getFieldDecorator('company', {
                rules: [{ required: false, message: '' }]
              })(<Input style={{ width: '100%' }} />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Form.Item label="Address1" hasFeedback>
              {getFieldDecorator('address1', {
                rules: [
                  { required: true, message: 'Please input your address' }
                ]
              })(<Input style={{ width: '100%' }} />)}
            </Form.Item>
          </div>
          <div className="col-md-6">
            <Form.Item label="Address2" hasFeedback>
              {getFieldDecorator('address2', {
                rules: [{ required: false, message: '' }]
              })(<Input style={{ width: '100%' }} />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Form.Item label="City" hasFeedback>
              {getFieldDecorator('city', {
                rules: [{ required: true, message: 'Please input your city' }]
              })(<Input style={{ width: '100%' }} />)}
            </Form.Item>
          </div>
          <div className="col-md-6">
            <Form.Item label="Post Code" hasFeedback>
              {getFieldDecorator('postcode', {
                rules: [
                  { required: true, message: 'Please input your post code' }
                ]
              })(<Input style={{ width: '100%' }} />)}
            </Form.Item>
          </div>
        </div>
        <div className="row">
          <div className="col-md-6">
            <Form.Item label="Country" hasFeedback>
              {getFieldDecorator('country', {
                rules: [
                  { required: true, message: 'Please choose your country' }
                ]
              })(
                <Select
                  style={{ width: '100%' }}
                  placeholder="--Please Select--"
                >
                  <Option value="Vietnam">Vietnam</Option>
                  <Option value="USA">USA</Option>
                  <Option value="Jajan">Jajan</Option>
                  <Option value="Korea">Korea</Option>
                  <Option value="China">China</Option>
                </Select>
              )}
            </Form.Item>
          </div>
          <div className="col-md-6">
            <Form.Item label="Region/State" hasFeedback>
              {getFieldDecorator('regionstate', {
                rules: [
                  { required: true, message: 'Please select your region/state' }
                ]
              })(
                <Select
                  style={{ width: '100%' }}
                  placeholder="--Please Select--"
                >
                  <Option value="Vietnam">Vietnam</Option>
                  <Option value="USA">USA</Option>
                  <Option value="Jajan">Jajan</Option>
                  <Option value="Korea">Korea</Option>
                  <Option value="China">China</Option>
                </Select>
              )}
            </Form.Item>
          </div>
        </div>
        <Form.Item>
          {getFieldDecorator('agreement', {})(
            <Checkbox
              onClick={this.handleCheckAgree}
              className={
                this.state.isCheckAgree !== false ? 'checked' : 'error-msg '
              }
            >
              I have read the <a href="/agreement">agreement</a>
            </Checkbox>
          )}
        </Form.Item>
        <Form.Item>
          <Button type="primary" htmlType="submit">
            Register
          </Button>
        </Form.Item>
      </Form>
    );
  }
}

const mapStateToProps = state => {
  return {
    accounts: state.accounts
  };
};
const mapDispatchToProps = dispatch => {
  return {
    register: () => {
      dispatch({ type: REGISTER });
    },
    loggin: () => {
      dispatch({ type: LOG_IN });
    },
    saveUserInfo: response => {
      dispatch({ type: GET_ACCOUNT, payload: { accounts: response } });
    }
  };
};

export const WrappedRegistrationForm = Form.create({ name: 'register' })(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(RegistrationForm)
);
