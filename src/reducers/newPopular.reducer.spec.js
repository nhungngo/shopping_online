import { GET_NEW_POPULAR } from '../actions/actions'
import { newPopular } from './newPopular.reducer'

describe('newPopular reducer', () => {
  it('should handle GET_NEW_POPULAR', () => {
    const action = {
      type: GET_NEW_POPULAR,
      payload: {
        newPopular: 'x'
      }
    }
    const state = []
    expect(newPopular(state, action)).toEqual(expect.arrayContaining(['x']))
  })
})
