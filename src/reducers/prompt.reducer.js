import { SHOW_PROMPT, HIDE_PROMPT } from "../actions/actions";

const initState = {
  show: false,
  title: '',
  content: ''
}

export const prompt = (state = initState, action) => {
  switch (action.type) {
    case SHOW_PROMPT:
      return {
        show: true,
        title: action.payload.title,
        content: action.payload.content
      };

    case HIDE_PROMPT:
      return {
        show: false,
        title: action.payload.title,
        content: action.payload.content
      };;

    default:
      return state;
  }
}
