import { accounts } from './account.reducer'
import { GET_ACCOUNT } from '../actions/actions'

describe('acount reducer', () => {
  it('should handle GET_ACCOUNT', () => {
    const action = {
      type: GET_ACCOUNT,
      payload: {
        accounts: ['x']
      }
    }
    const state = []
    expect(accounts(state, action)).toEqual(expect.arrayContaining(['x']))
  })
})
