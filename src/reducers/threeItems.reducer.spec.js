import { GET_THREE_ITEMS } from '../actions/actions'
import { threeItems } from './threeItems.reducer'

describe('threeItems reducer', () => {
  it('should handle GET_THREE_ITEMS', () => {
    const action = {
      type: GET_THREE_ITEMS,
      payload: {
        threeItems: ['x']
      }
    }
    let state = []
    expect(threeItems(state, action)).toEqual(expect.arrayContaining(['x']))
  })
})
