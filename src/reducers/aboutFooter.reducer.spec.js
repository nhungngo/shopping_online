import { GET_ABOUT } from '../actions/actions'
import { abouts } from './aboutFooter.reducer'

describe('counter reducer', () => {
  it('should handle GET_ABOUT', () => {
    const action = {
      type: GET_ABOUT,
      payload: {
        abouts: ['x']
      }
    }
    const state = []
    expect(abouts(state, action)).toEqual(expect.arrayContaining(['x']))
  })
})
