import { GET_API, POST_API, DELETE_API, PUT_API } from '../actions/actions'
import { products } from './product.reducer'

describe('products reducer', () => {
  it('should handle GET_PRODUCT_DETAIL', () => {
    const action = {
      type: GET_API,
      payload: {
        products: ['x']
      }
    }
    const state = []
    expect(products(state, action)).toEqual(expect.arrayContaining(['x']))
  })

  it('should handle POST_API', () => {
    const action = {
      type: POST_API,
      payload: {
        product: ['x']
      }
    }
    const state = []
    expect(products(state, action)).toEqual(
      expect.arrayContaining([expect.arrayContaining([], ['x'])])
    )
  })
})
