import { GET_NEW_ITEM } from '../actions/actions';

export const newItems = (state = [], action) => {
  switch (action.type) {
    case GET_NEW_ITEM:
     return [...action.payload.newItems]
    
    default:
      return state
  }
}