import * as type from '../actions/actions'
import { filter } from './filter.reducer'

describe('filter reducer', () => {
  it('should return the initial state', () => {
    expect(filter(undefined, {})).toEqual({
      not_available: false,
      in_stock: false,
      price_from: 0,
      price_to: 100
    })
  })

  it('should handle GET_NOT_AVAILABLE', () => {
    const action = {
      type: type.GET_NOT_AVAILABLE,
      payload: {
        not_available: true
      }
    }
    const state = {}
    expect(filter(state, action)).toEqual(
      expect.objectContaining({ not_available: true })
    )
  })

  it('should handle GET_IN_STOCK', () => {
    const action = {
      type: type.GET_IN_STOCK,
      payload: {
        in_stock: false
      }
    }
    const state = {}
    expect(filter(state, action)).toEqual(
      expect.objectContaining({ in_stock: false })
    )
  })

  it('should handle RANGE', () => {
    const action = {
      type: type.RANGE,
      payload: {
        price_from: 50,
        price_to: 100
      }
    }
    const state = {}
    expect(filter(state, action)).toEqual(
      expect.objectContaining({ price_from: 50, price_to: 100 })
    )
  })
})
