import { basket } from './basket.reducer'

describe('basket reducer', () => {
  it('should handle BASKET_ADD_PRODUCT', () => {
    const action = {
      type: 'BASKET_ADD_PRODUCT'
    }
    const state = []
    expect(basket(state, action)).toEqual(expect.arrayContaining([]))
  })

  it('should handle BASKET_REMOVE_PRODUCT', () => {
    const action = {
      type: 'BASKET_REMOVE_PRODUCT'
    }
    const state = []
    expect(basket(state, action)).toEqual(expect.arrayContaining([]))
  })
})
