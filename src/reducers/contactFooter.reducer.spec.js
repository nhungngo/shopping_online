import { GET_CONTACT } from '../actions/actions'
import { contacts } from './contactFooter.reducer'

describe('contacts reducer', () => {
  it('should handle GET_CONTACT', () => {
    const action = {
      type: GET_CONTACT,
      payload: {
        contacts: ['x']
      }
    }
    const state = []
    expect(contacts(state, action)).toEqual(expect.arrayContaining(['x']))
  })
})
