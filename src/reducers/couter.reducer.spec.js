import * as types from '../actions/actions'
import { counter } from './couter.reducer'

describe('counter reducer', () => {
  it('should handle INCREASE', () => {
    const action = {
      type: types.INCREASE,
      payload: {
        step: 1
      }
    }
    const state = 0

    expect(counter(state, action)).toBe(1)
  })

  it('should handle DECREASE', () => {
    const action = {
      type: types.DECREASE,
      payload: {
        step: 1
      }
    }
    const state = 3

    expect(counter(state, action)).toBe(2)
  })
})
