import { SEARCH } from '../actions/actions'

export const search = (state = '', action) => {
  switch (action.type) {
    case SEARCH:
      return action.payload.search
    default:
      return state
  }
}
