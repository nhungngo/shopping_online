import * as type from '../actions/actions'
import { sort } from './sort.reducer'

describe('sort reducer', () => {
  it('should return the initial state', () => {
    expect(sort(undefined, {})).toEqual({
      sort: 'default',
      show: 3
    })
  })

  it('should handle SORT', () => {
    const action = {
      type: type.SORT,
      payload: {
        sort: ['x']
      }
    }
    const state = {}
    expect(sort(state, action)).toEqual(
      expect.objectContaining({ sort: ['x'] })
    )
  })

  it('should handle SHOW', () => {
    const action = {
      type: type.SHOW,
      payload: {
        show: 10
      }
    }
    const state = {}
    expect(sort(state, action)).toEqual(expect.objectContaining({ show: 10 }))
  })
})
