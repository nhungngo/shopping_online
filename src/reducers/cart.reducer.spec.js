import { GET_CART, DELETE_CART_ITEM } from '../actions/actions'
import { carts } from './cart.reducer'

describe('carts reducer', () => {
  it('should handle GET_CART', () => {
    const action = {
      type: GET_CART,
      payload: {
        carts: ['x']
      }
    }
    const state = []
    expect(carts(state, action)).toEqual(expect.arrayContaining(['x']))
  })

  it('should handle GET_BEST_SELLER', () => {
    const action = {
      type: DELETE_CART_ITEM,
      payload: {
        cartId: 1
      }
    }
    const state = [{ id: 1 }, { id: 2 }]

    expect(carts(state, action)).toEqual(
      expect.arrayContaining([
        expect.not.objectContaining({
          id: 1
        })
      ])
    )
  })
})
