import { SHOW_LOADING, HIDE_LOADING } from '../actions/actions'
import { loading } from './loading.reducer'

describe('loading reducer', () => {
  it('should handle SHOW_LOADING', () => {
    const action = {
      type: SHOW_LOADING
    }
    let state = false
    expect(loading(state, action)).toBeTruthy()
  })

  it('should handle HIDE_LOADING', () => {
    const action = {
      type: HIDE_LOADING
    }
    let state = true
    expect(loading(state, action)).toBeFalsy()
  })
})
