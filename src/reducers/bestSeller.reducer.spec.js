import { bestSellers } from './bestSeller.reducer'
import { GET_BEST_SELLER } from '../actions/actions'
// fail
describe('bestSellers reducer', () => {
  it('should handle GET_BEST_SELLER', () => {
    const action = {
      type: GET_BEST_SELLER,
      payload: {
        bestSellers: ['ab']
      }
    }
    const state = []
    expect(bestSellers(state, action)).toEqual(expect.arrayContaining(['ab']))
  })
})
