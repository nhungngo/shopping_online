import { REGISTER } from '../actions/actions'
import { register } from './register.reducer'

describe('register reducer', () => {
  it('should handle REGISTER', () => {
    const action = {
      type: REGISTER
    }
    let state = false
    expect(register(state, action)).toBeTruthy()
  })
})
