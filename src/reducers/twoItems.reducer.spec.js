import { GET_TWO_ITEMS } from '../actions/actions'

import { twoItems } from './twoItems.reducer'

describe('twoItems reducer', () => {
  it('should handle GET_TWO_ITEMS', () => {
    const action = {
      type: GET_TWO_ITEMS,
      payload: {
        twoItems: ['x']
      }
    }
    let state = []
    expect(twoItems(state, action)).toEqual(expect.arrayContaining(['x']))
  })
})
