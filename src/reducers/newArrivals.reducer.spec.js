import { GET_NEW_ARRIVALS } from '../actions/actions'
import { newArrivals } from './newArrivals.reducer'

describe('newArrivals reducer', () => {
  it('should handle GET_NEW_ARRIVALS', () => {
    const action = {
      type: GET_NEW_ARRIVALS,
      payload: {
        newArrivals: ['x']
      }
    }
    const state = []
    expect(newArrivals(state, action)).toEqual(expect.arrayContaining(['x']))
  })
})
