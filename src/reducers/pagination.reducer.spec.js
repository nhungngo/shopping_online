import * as type from '../actions/actions'
import { pagination } from './pagination.reducer'

describe('pagination reducer', () => {
  it('should handle PAGINATION', () => {
    const action = {
      type: type.PAGINATION,
      payload: {
        current: 2
      }
    }
    const state = 1
    expect(pagination(state, action)).toEqual(2)
  })
})
