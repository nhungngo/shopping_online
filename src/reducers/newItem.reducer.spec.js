import { GET_NEW_ITEM } from '../actions/actions'
import { newItems } from './newItem.reducer'

describe('newItems reducer', () => {
  it('should handle GET_NEW_ITEM', () => {
    const action = {
      type: GET_NEW_ITEM,
      payload: {
        newItems: ['x']
      }
    }
    const state = []
    expect(newItems(state, action)).toEqual(expect.arrayContaining(['x']))
  })
})
