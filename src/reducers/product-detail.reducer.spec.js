import * as type from '../actions/actions'
import { productDetail } from './product-detail.reducer'

describe('productDetail reducer', () => {
  it('should handle GET_PRODUCT_DETAIL', () => {
    const action = {
      type: type.GET_PRODUCT_DETAIL,
      payload: {
        product: { id: 1 }
      }
    }
    const state = {}
    expect(productDetail(state, action)).toEqual(
      expect.objectContaining({ id: 1 })
    )
  })

  it('should handle UPDATE_PRODUCT', () => {
    const action = {
      type: type.UPDATE_PRODUCT,
      payload: {
        product: { id: 2 }
      }
    }
    const state = {}
    expect(productDetail(state, action)).toEqual(
      expect.objectContaining({ id: 2 })
    )
  })
})
