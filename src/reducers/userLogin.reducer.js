import { GET_USER } from '../actions/actions'

export const user = (state = [], action) => {
  switch (action.type) {
    case GET_USER:
      return [action.payload.user]

    default:
      return state
  }
}
