import { LOG_IN, LOG_OUT } from '../actions/actions'
import { isLogin } from './login.reducer'

describe('isLogin reducer', () => {
  it('should handle LOG_IN', () => {
    const action = {
      type: LOG_IN
    }
    let state = false
    expect(isLogin(state, action)).toBeTruthy()
  })

  it('should handle LOG_OUT', () => {
    const action = {
      type: LOG_OUT
    }
    let state = true
    expect(isLogin(state, action)).toBeFalsy()
  })
})
