import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import CartHeader from '../../components/cart-header/cart-header.container'
import HeaderNavComponent from '../../components/header-nav/header-nav.component'
import { Authentication } from '../../services/authen.service'
import * as action from './../../actions/actions'

class HeaderContainer extends Component {

  logout = (e) => {
    e.preventDefault();
    if (localStorage) {
      localStorage.clear()
    }
    Authentication.logout()
  }

  handleLink = () => {
    this.props.onSearch({
      search: ''
    })
  }

  render() {
    return (
      <>
        <div className="pre-header">
          <div className="container">
            <div className="row">
              <div className="col-md-6 col-sm-6 additional-shop-info">
                <ul className="list-unstyled list-inline">
                  <li>
                    <i className="fa fa-phone" />
                    <span>+1 456 6717</span>
                  </li>
                </ul>
              </div>
              <div className="col-md-6 col-sm-6 additional-nav">
                <ul className="list-unstyled list-inline pull-right">
                  <li>
                    <Link onClick={this.handleLink} to="/checkout">
                      Checkout
                    </Link>
                  </li>
                  {this.props.isLogin ? (
                    <>
                      <li>
                        <Link onClick={this.handleLink} to="/account">
                          My Account
                        </Link>
                      </li>
                      <li>
                        <Link onClick={(e) => this.logout(e)} to="/">
                          Logout
                        </Link>
                      </li>
                    </>
                  ) : (
                    <>
                      <li>
                        <Link onClick={this.handleLink} to="/register">
                          Register
                        </Link>
                      </li>
                      <li>
                        <Link onClick={this.handleLink} to="/login">
                          Log In
                        </Link>
                      </li>
                    </>
                  )}
                </ul>
              </div>
            </div>
          </div>
        </div>

        <div className="header">
          <div className="container">
            <Link onClick={this.handleLink} className="site-logo" to="/">
              <img
                src="/assets/corporate/img/logos/logo-shop-red.png"
                alt="Metronic Shop UI"
              />
            </Link>

            <Link onClick={this.handleLink} to="/" className="mobi-toggler">
              <i className="fa fa-bars" />
            </Link>

            <CartHeader />
            <HeaderNavComponent />
          </div>
        </div>
      </>
    )
  }
}

const mapStateToProps = state => {
  return {
    isLogin: state.isLogin,
    searchKey: state.search
  }
}
const mapDispatchToProps = (dispatch, props) => {
  return {
    onSearch: response => {
      dispatch({ type: action.SEARCH, payload: { search: response.search } })
    }
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderContainer)
