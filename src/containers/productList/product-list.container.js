import React, { Component } from 'react'
import { VerticalCategory } from '../../components/vertical-category/vertical-category.container'
import './product-list.container.scss'
import ProductItem from '../../components/product-item/product-item.component'
import { http } from '../../services/http.service'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import { GET_API, GET_CART } from '../../actions/actions'
import SortProductComponent from '../../components/sortProduct/sort-product.component'
import FilterProductComponent from '../../components/filterProduct/filter-product.component'
import BestSellerComponent from '../../components/bestSeller/best-seller.component'
import PaginationComponent from '../../components/pagination/pagination.component'
import BreadcrumbComponent from '../../components/breadcrumb/breadcrumb.component'
import { Empty } from 'antd'
import { FilterService } from '../../services/filter.service'
import PropTypes from 'prop-types'
import SearchComponent from '../../components/search/search.component'

class ProductListContainer extends Component {
  componentDidMount = () => {
    this.getProducts()
  }
  getProducts() {
    http.get('products').then(res => {
      this.props.getProducts(res.data)
    })
  }
  render() {
    let productList = this.props.products
    let { filter, display, current, searchKey } = this.props
    let pathname = this.props.location.pathname.slice(1)
    let path = pathname.slice(13)
    let productResults = productList
    if (pathname === 'search') {
      if (searchKey === '') {
        productResults = []
      } else {
        productResults = FilterService.search(productList, searchKey)
      }
    } else {
      if (path !== '') {
        productResults = productList.filter(p => {
          return p.category === path
        })
      }
    }
    let filteredProduct = FilterService.filterProduct(filter, productResults)
    let sortedProduct = FilterService.sortProduct(display, filteredProduct)
    let showedProduct = FilterService.showProduct(
      current,
      display.show,
      sortedProduct
    )
    let avai = filteredProduct.filter(product => {
      return product.availability === 'not available'
    }).length
    let stock = filteredProduct.filter(product => {
      return product.availability === 'in stock'
    }).length
    let imgBanner = `/assets/pages/img/title-bg/${path}-bg.jpg`
    let category = path.replace('_', ' ')
    let routesConfig = [
      {
        path: '/product_list',
        breadcrumbName: 'store'
      }
    ]
    let styleBackground = {
      background: `url(${imgBanner}) no-repeat right center`,
      backgroundSize: 'cover'
    }
    if (showedProduct.length === 0) {
      current = 1
      showedProduct = FilterService.showProduct(
        current,
        display.show,
        sortedProduct
      )
    }
    if (pathname !== 'search' && searchKey !== '') {
      return <Redirect to="/search" />
    }
    if (pathname === 'search') {
      routesConfig = [
        ...routesConfig,
        {
          path: '/search',
          breadcrumbName: 'search result'
        }
      ]
    }
    if (path !== '') {
      routesConfig = [
        ...routesConfig,
        {
          path: path,
          breadcrumbName: category
        }
      ]
    } else {
      category = 'all'
    }

    return (
      <div className="main product-list">
        {pathname === 'search' ? (
          ''
        ) : (
          <div className="product-list__banner" style={styleBackground}>
            <div className="product-list__banner__content">
              <div className="container">
                <div className="product-list__banner__content__wrap">
                  <div className="product-list__banner__content__title">
                    <span className="product-list__banner__content__title--red">
                      {category}
                    </span>
                    category
                  </div>
                  <div className="product-list__banner__content__detail">
                    Over {productResults.length} Items are available here
                  </div>
                </div>
              </div>
            </div>
          </div>
        )}
        <div className="container">
          <BreadcrumbComponent routesConfig={routesConfig} />
          <div className="row margin-bottom-40">
            <div className="col-lg-3 mobile-none">
              <VerticalCategory />
              <FilterProductComponent avai={avai} stock={stock} />
              <BestSellerComponent />
            </div>
            <div className="col-lg-9 ">
              {pathname !== 'search' ? (
                ''
              ) : (
                <div className="product-list__search">
                  <h1>
                    Search result for{' '}
                    <span className="product-list__search--search-key">
                      {searchKey === '' ? `''` : searchKey}
                    </span>
                  </h1>
                  <div className="product-list__search__wrap">
                    <SearchComponent placeholder={'Search again'} />
                  </div>
                </div>
              )}
              <div className="product-list__sort">
                <SortProductComponent />
              </div>
              <div className="product-list__wrap">
                <div className="product-list__result">
                  {showedProduct.map((val, i) => (
                    <div className="col-sm-4" key={i}>
                      <ProductItem
                        inforItem={val}
                        styleWith={'100%'}
                        addToCart={this.addToCart}
                      />
                    </div>
                  ))}
                </div>
                {sortedProduct.length === 0 ? (
                  <div className="product-list__none">
                    <Empty description={'NO PRODUCT'} />
                  </div>
                ) : (
                  <PaginationComponent productTotal={filteredProduct} />
                )}
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    products: state.products,
    display: state.sort,
    filter: state.filter,
    current: state.pagination,
    searchKey: state.search
  }
}

const mapDispatchToProps = dispatch => {
  return {
    getProducts: response =>
      dispatch({ type: GET_API, payload: { products: response } }),
    addToCart: response =>
      dispatch({ type: GET_CART, payload: { carts: response } })
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductListContainer)

ProductListContainer.propTypes = {
  products: PropTypes.array,
  display: PropTypes.shape({
    sort: PropTypes.string,
    show: PropTypes.number
  }),
  current: PropTypes.number,
  searchKey: PropTypes.string,
  path: PropTypes.string
}
ProductListContainer.defaultProps = {
  products: [],
  display: {
    sort: 'default',
    show: 9
  },
  current: 1,
  searchKey: '',
  path: ''
}
