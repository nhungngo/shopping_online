import React, { Component } from 'react'
import { VerticalCategory } from '../../components/vertical-category/vertical-category.container'
import ProductItem from '../../components/product-item/product-item.component'
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom'
import CarouselComponent from '../../components/carousel/carousel.component'
import { http } from '../../services/http.service'
import './home.container.scss'
import MainCarouselComponent from '../../components/mainCarousel/main-carousel.component'
import { GET_API } from '../../actions/actions'

function ContentCarousel(props) {
  return (
    <div className=" item active">
      <img
        src={props.imgLink}
        className="img-responsive"
        alt="Berry Lace Dress"
      />
    </div>
  )
}
const imgArray = [
  '/assets/pages/img/index-sliders/slide1.jpg',
  '/assets/pages/img/index-sliders/slide2.jpg',
  '/assets/pages/img/index-sliders/slide3.jpg'
]
class HomeContainer extends Component {
  componentDidMount = () => {
    this.getProducts()
  }
  getProducts = () => {
    http.get('products').then(res => {
      this.props.getProducts(res.data)
    })
  }
  render() {
    if (this.props.searchKey !== '') {
      return <Redirect to="/search" />
    }
    const pictureCarouselConfig = {
      dots: true,
      autoplay: true,
      autoplaySpeed: 4000,
      cssEase: 'linear',
      slidesToShow: 1,
      slidesToScroll: 1,
      responsive: []
    }
    const threeItemCarouselConfig = {
      slidesToShow: 3,
      slidesToScroll: 3,
      responsive: [
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    }
    const twoItemCarouselConfig = {
      ...threeItemCarouselConfig,
      slidesToShow: 2,
      slidesToScroll: 2
    }
    let pictureCarouselContent = imgArray.map((val, i) => (
      <ContentCarousel imgLink={val} key={i} />
    ))
    let newArrivalCarouselContent = this.props.products
      .filter(p => {
        return p.category === 'new-arrival'
      })
      .map((val, i) => (
        <ProductItem inforItem={val} key={i} styleWith={'100%'} />
      ))
    let threeItemCarouselContent = this.props.products
      .filter(p => {
        return p.category === 'three-item'
      })
      .map((val, i) => (
        <ProductItem inforItem={val} key={i} styleWith={'100%'} />
      ))

    let twoItemCarouselContent = this.props.products
      .filter(p => {
        return p.category === 'two-item'
      })
      .map((val, i) => (
        <ProductItem inforItem={val} key={i} styleWith={'100%'} />
      ))
    return (
      <>
        {<MainCarouselComponent />}
        <div className="main">
          <div className="container">
            <div className="row margin-bottom-40">
              <div className="col-md-12 sale-product">
                <h2>New Arrivals</h2>
                <CarouselComponent
                  carouselContent={newArrivalCarouselContent}
                />
              </div>
            </div>
            <div className="row margin-bottom-40">
              {<VerticalCategory />}
              <div className="col-md-9 col-sm-8">
                <h2>Three items</h2>
                <CarouselComponent
                  carouselContent={threeItemCarouselContent}
                  config={threeItemCarouselConfig}
                />
              </div>
            </div>
            <div className="row margin-bottom-35">
              <div className="col-lg-6 mb-3 mb-lg-0">
                <div className="two-items-bottom-items">
                  <h2>Two items</h2>
                  <CarouselComponent
                    carouselContent={twoItemCarouselContent}
                    config={twoItemCarouselConfig}
                  />
                </div>
              </div>
              <div className="col-lg-6 ">
                <div className="picture-carousel">
                  <CarouselComponent
                    carouselContent={pictureCarouselContent}
                    config={pictureCarouselConfig}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </>
    )
  }
}
const mapStateToProps = state => {
  return {
    products: state.products,
    searchKey: state.search
  }
}
const mapDispatchToProps = dispatch => {
  return {
    getProducts: response =>
      dispatch({ type: GET_API, payload: { products: response } })
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeContainer)
