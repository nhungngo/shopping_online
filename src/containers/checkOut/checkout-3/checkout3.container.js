import React, { Component } from 'react';
import './checkout3.container.css';
import Form from 'react-validation/build/form';
import Input from 'react-validation/build/input';
import Select from 'react-validation/build/select';
import CheckButton from 'react-validation/build/button';
import { isEmail, isEmpty } from 'validator';
import PropTypes from 'prop-types';

const required = value => {
  if (isEmpty(value)) {
    return <small className="messCheckout3">This field is required</small>;
  }
};
const email = value => {
  if (!isEmail(value)) {
    return <small className="messCheckout3">Invalid email format</small>;
  }
};

class Checkout3 extends Component {
  submited = false;
  constructor(props) {
    super(props);
    this.state = {
      fname: '',
      lname: '',
      email: '',
      phone: '',
      fax: '',
      company: '',
      address1: '',
      address2: '',
      city: '',
      postCode: '',
      country: '',
      regionState: ''
    };
  }
  componentWillMount() {
    if (localStorage) {
      let user = JSON.parse(localStorage.getItem('userInfor'));
      this.setState({
        user
      });
    }
  }

  handleSubmit = e => {
    e.preventDefault();
    this.form.validateAll();
    this.submited = true;
    if (this.checkBtn.context._errors.length === 0) {
      this.props.deliveryDetails(this.state, this.submited);
    }
  };

  handleChange = e => {
    this.submited = false;
    var { target } = e;
    var { name, value } = target;
    this.setState({
      [name]: value
    });
    var obj = {
      ...this.state,
      [name]: value
    };
    this.props.deliveryDetails({ ...obj }, this.submited);
  };

  render() {
    // var info = this.props.info ? this.props.info : [];
    let info = this.state.user ? this.state.user : {};
    return (
      <Form
        className="container"
        onSubmit={e => this.handleSubmit(e)}
        ref={c => {
          this.form = c;
        }}
      >
        <div className="row">
          <div className="col-md-6">
            <div className="form-group">
              <label>
                First Name <span className="star">*</span>
              </label>
              <Input
                type="text"
                className="form-control"
                name="fname"
                onChange={this.handleChange}
                value={info.firstname}
                validations={[required]}
              />
            </div>
            <div className="form-group">
              <label>
                Last Name <span className="star">*</span>
              </label>
              <Input
                type="text"
                className="form-control"
                name="lname"
                onChange={this.handleChange}
                value={info.lastname}
                validations={[required]}
              />
            </div>
            <div className="form-group">
              <label>
                E-Mail <span className="star">*</span>
              </label>
              <Input
                type="email"
                className="form-control"
                name="email"
                onChange={this.handleChange}
                value={info.email}
                validations={[required, email]}
              />
            </div>
            <div className="form-group">
              <label>
                Telephone <span className="star">*</span>
              </label>
              <Input
                type="number"
                className="form-control"
                name="phone"
                onChange={this.handleChange}
                value={info.phone}
                validations={[required]}
              />
            </div>
            <div className="form-group">
              <label>Fax</label>
              <Input
                type="text"
                className="form-control"
                name="fax"
                onChange={this.handleChange}
                value={info.fax}
              />
            </div>
            <div className="form-group">
              <label>Company</label>
              <Input
                type="text"
                className="form-control"
                name="company"
                onChange={this.handleChange}
                value={info.company}
              />
            </div>
          </div>

          <div className="col-md-6">
            <div className="form-group">
              <label>Address 1</label>
              <Input
                type="text"
                className="form-control"
                name="address1"
                onChange={this.handleChange}
                value={info.address1}
              />
            </div>
            <div className="form-group">
              <label>Address 2</label>
              <Input
                type="text"
                className="form-control"
                name="address2"
                onChange={this.handleChange}
                value={info.address2}
              />
            </div>
            <div className="form-group">
              <label>
                City <span className="star">*</span>
              </label>
              <Input
                type="text"
                className="form-control"
                name="city"
                onChange={this.handleChange}
                value={info.city}
                validations={[required]}
              />
            </div>
            <div className="form-group">
              <label>
                Post Code <span className="star">*</span>
              </label>
              <Input
                type="text"
                className="form-control"
                name="postCode"
                onChange={this.handleChange}
                value={info.postcode}
                validations={[required]}
              />
            </div>
            <div className="form-group">
              <label>
                Country <span className="star">*</span>
              </label>
              <Select
                className="form-control"
                name="country"
                onChange={this.handleChange}
                validations={[required]}
                value={info.country}
              >
                <option>--- Please Select ---</option>
                <option value={info.country}>{info.country}</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </Select>
            </div>
            <div className="form-group">
              <label>
                Region/State <span className="star">*</span>
              </label>
              <Select
                className="form-control"
                name="regionState"
                onChange={this.handleChange}
                validations={[required]}
                value={info.regionstate}
              >
                <option>--- Please Select ---</option>
                <option value={info.regionstate}>{info.regionstate}</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
              </Select>
            </div>
          </div>
        </div>
        <button type="submit" value="submit" className="btn btn-checkout3">
          CONTINUE
        </button>
        <CheckButton
          style={{ display: 'none' }}
          ref={c => {
            this.checkBtn = c;
          }}
        />
      </Form>
    );
  }
}

export default Checkout3;

Checkout3.propTypes = {
  info: PropTypes.object
};
Checkout3.defaultProps = {
  info: {}
};
