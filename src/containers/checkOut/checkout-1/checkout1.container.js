import React from 'react'
import '../checkout1-2.scss'
import { withFormik, Form, Field } from 'formik'
import { Link } from "react-router-dom";
import * as yup from 'yup'
import CheckoutLoginContainer from './checkout-login.container'

const CheckOut1Form = props => (
  <div>
    <div className="row">
      <div className="col-md-6">
        <h3>New Customer</h3>
        <p>Checkout Options:</p>

        <Form>
          <div className="form-check">
            <Field
              className="form-check-input"
              type="radio"
              name="customerRadios"
              id="customerRadios1"
              value="registerAccount"
            />
            <label className="form-check-label" htmlFor="customerRadios1">
              Register Account
            </label>
          </div>
          <div className="form-check">
            <Field
              className="form-check-input"
              type="radio"
              name="customerRadios"
              id="customerRadios2"
              value="guestCheckout"
            />
            <label className="form-check-label" htmlFor="customerRadios2">
              Guest Checkout
            </label>
          </div>
          {props.errors.customerRadios && (
            <p className="red">{props.errors.customerRadios}</p>
          )}

          <p>
            By creating an account you will be able to shop faster, be up to
            date on an other's status, and keep track of the orders you have
            privious made.
          </p>

          <button className="btn" type="submit">
            Continue
          </button>
        </Form>
      </div>
      <div className="col-md-6">
        <h3>Returning Customer</h3>
        <p>I am a returning customer.</p>
        <div className="login-checkout">
          <CheckoutLoginContainer />
        </div>
        <div className="login-socio">
          <p className="text-muted">or login using:</p>
          <ul className="social-icons">
            <li>
              <Link
                to=""
                data-original-title="facebook"
                className="facebook"></Link>
            </li>
            <li>
              <Link
                to=""
                data-original-title="Twitter"
                className="twitter"></Link>
            </li>
            <li>
              <Link
                to=""
                data-original-title="Google Plus"
                className="googleplus"></Link>
            </li>
            <li>
              <Link
                to=""
                data-original-title="Linkedin"
                className="LinkedIn"></Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
)

const Checkout1 = withFormik({
  mapPropsToValues({ customerRadios }) {
    return {
      customerRadios: customerRadios || ''
    }
  },
  validationSchema: yup.object().shape({
    customerRadios: yup.string().required('You have to choose one option')
  }),
  handleSubmit(values, checkoutOption) {
    checkoutOption.props.checkoutOption({
      options: values.customerRadios
    })
  }
})(CheckOut1Form)

export default Checkout1
