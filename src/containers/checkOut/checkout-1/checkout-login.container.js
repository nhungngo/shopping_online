import React, { Component } from "react"
import "antd/dist/antd.css"
import { WrappedLoginForm } from "../../../components/login/login.component"
import { connect } from "react-redux"
import { Redirect } from "react-router-dom"

class CheckoutLoginContainer extends Component {
  render() {
    if (this.props.isLogin) {
      return <Redirect to="/checkout" />
    }
    return (
      <div className="container login">
        <WrappedLoginForm />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return { isLogin: state.isLogin }
}

export default connect(
  mapStateToProps,
  null
)(CheckoutLoginContainer)
