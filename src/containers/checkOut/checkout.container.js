import React, { Component } from "react"
import { Collapse } from "antd"
import "./checkOut.container.scss"
import { connect } from "react-redux"
import { http } from '../../services/http.service';

import Checkout1 from './checkout-1/checkout1.container';
import Checkout2 from './checkout-2/checkout2.container';
import CheckOut3 from './checkout-3/checkout3.container';
import CheckOut4 from './checkout-4/checkout4.container';
import Checkout5 from './checkout-5/checkout5.container';
import Checkout6 from './checkout-6/checkout-6.container';

const Panel = Collapse.Panel;

class CheckOut extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showStep1: false,
      showStep2: false,
      showStep3: false,
      showStep4: false,
      showStep5: false,
      showStep6: false,
      valid2: false,
      valid3: false,
      valid4: false,
      valid5: false
    };
  }

  componentWillMount = () => {
    var { isLogin } = this.props;

    if (isLogin) {
      this.setState({
        defaultKey: 3,
        showStep3: true,
        activeKey: ['3']
      });
    } else {
      this.setState({
        defaultKey: 1,
        activeKey: ['1'],
        showStep1: true
      });
    }
  };

  componentWillReceiveProps = () => {
    var { isLogin } = this.props;
    if (isLogin) {
      this.setState({
        showStep1: false,
        showStep2: false,
        showStep3: true,
        activeKey: ['3']
      });
    } else {
      this.setState({
        showStep1: true,
        activeKey: ['1']
      });
    }
  };

  // params checkout 1
  checkoutOption = options => {
    if (options.options === 'registerAccount') {
      this.setState({
        showStep2: true,
        activeKey: ['2']
      });
    } else {
      this.setState({
        showStep3: true,
        activeKey: ['3']
      });
    }
  };

  // params checkout 2
  accountDetail = info => {
    this.setState({
      checkout2: info,
      showStep3: true,
      activeKey: ['3']
    });
  };

  //params checkout 3
  deliveryDetails = (params, submited) => {
    if (submited) {
      this.setState({
        checkout3: params,
        valid3: true,
        showStep4: true,
        activeKey: ['4']
      });
    } else {
      this.setState({
        checkout3: params,
        valid3: false
      });
    }
  };

  //params checkout 4
  deliveryMethod = (params, submited) => {
    if (submited && params.typeShippingRate) {
      this.setState({
        checkout4: params,
        valid4: true,
        showStep5: true,
        activeKey: ['5']
      });
    } else {
      this.setState({
        checkout4: params,
        valid4: false
      });
    }
  };

  paymentMethod = (params, submited) => {
    if (submited && params.cashOnDelivery && params.agree) {
      this.setState({
        checkout5: params,
        valid5: true,
        showStep6: true,
        activeKey: ['6']
      });
    } else {
      this.setState({
        checkout5: params,
        valid5: params.cashOnDelivery && params.agree
      });
    }
  };

  confirmOrder = (carts, finalTotal) => {
    let { valid5, valid4, valid3 } = this.state;
    if (carts.length)
      if (valid5 && valid4 && valid3) {
        let idCarts = carts.map(item => item.id)
        for (let i = 0; i < idCarts.length; i++) {
          this.props.history.push({
            pathname: '/printBill',
            data: {
              ...this.state,
              checkout6: { carts, finalTotal }
            }
          })
          http.delete(`carts/${idCarts[i]}`).then()
        }

      } else {
        console.log('hay validate o checkout5,4,3');
        alert('Moi kiem tra lai cac step');
        if (!valid3)
          this.setState({ showStep3: true })
        if (!valid4)
          this.setState({ showStep4: true })
        if (!valid5)
          this.setState({ showStep5: true })
      }
    else {
      alert('Gio hang trong, moi chon san pham muon mua')
      this.props.history.push('/product_list')
    }
  };

  callback = key => {
    this.setState({
      activeKey: key
    });
  };
  render() {
    return (
      <>
        <h2 className="text-uppercase">checkout</h2>
        <div className="frame-checkout my-3">
          <Collapse activeKey={this.state.activeKey} onChange={this.callback}>
            <Panel
              className="title"
              id="checkout1"
              header="step 1: checkout options"
              key="1"
              showArrow={false}
              disabled={!this.state.showStep1}
            >
              <div className="contents">
                <Checkout1
                  checkoutOption={this.checkoutOption}
                  login={this.login}
                />
              </div>
            </Panel>
            <Panel
              className="title"
              id="checkout2"
              header="step 2: account & billing details"
              key="2"
              showArrow={false}
              disabled={!this.state.showStep2}
            >
              <div className="contents">
                <Checkout2 accountDetail={this.accountDetail} />
              </div>
            </Panel>
            <Panel
              className="title"
              id="checkout3"
              header="step 3: delivery details"
              key="3"
              showArrow={false}
              disabled={!this.state.showStep3}
            >
              <div className="contents">
                <CheckOut3
                  deliveryDetails={this.deliveryDetails}
                  info={this.state.checkout2}
                />
              </div>
            </Panel>
            <Panel
              className="title"
              id="checkout4"
              header="step 4: delivery method"
              key="4"
              showArrow={false}
              disabled={!this.state.showStep4}
            >
              <div className="contents">
                <CheckOut4 deliveryMethod={this.deliveryMethod} />
              </div>
            </Panel>
            <Panel
              className="title"
              id="checkout5"
              header="step 5: payment method"
              key="5"
              showArrow={false}
              disabled={!this.state.showStep5}
            >
              <div className="contents">
                <Checkout5 paymentMethod={this.paymentMethod} />
              </div>
            </Panel>
            <Panel
              className="title"
              id="checkout6"
              header="step 6: confirm order"
              key="6"
              showArrow={false}
              disabled={!this.state.showStep6}
            >
              <div className="contents">
                <Checkout6 confirmOrder={this.confirmOrder} />
              </div>
            </Panel>
          </Collapse>
        </div>
      </>
    );
  }
}

const mapStateToProps = state => {
  return {
    isLogin: state.isLogin
  };
};

export default connect(
  mapStateToProps,
  null
)(CheckOut);
