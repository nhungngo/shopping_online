import React from "react"
import Form from "react-validation/build/form"
import Textarea from "react-validation/build/textarea"
import "./checkout4.container.css"
import CheckButton from "react-validation/build/button"

const maxLength = value => {
  if (value.trim().length > 200) {
    return (
      <small className="messCheckout4">
        Comment must be at less than 200 characters long!
      </small>
    )
  }
}

class Checkout4 extends React.Component {
  submited = false;
  state = {
    comment: "",
    typeShippingRate: ""
  }

  handleSubmit = event => {
    event.preventDefault();
    this.submited = true;
    this.props.deliveryMethod(this.state, this.submited);
  }

  handleChangeComment = event => {

    this.submited = false;
    this.setState({
      comment: event.target.value
    })
    this.props.deliveryMethod(null, this.submited);
  }

  handleChangeTypeShippingRate = event => {

    this.submited = false;
    this.setState({
      typeShippingRate: event.target.value
    })
    this.props.deliveryMethod(null, this.submited);
  }

  render() {
    return (
      <Form
        className="container"
        onSubmit={e => this.handleSubmit(e)}
        ref={c => {
          this.form = c
        }}
      >
        <p>
          Please select the preferred shipping method to use on this order.{" "}
        </p>
        <h3> Flat Rate </h3>
        <div>
          <input
            type="radio"
            id="radio1"
            name="typeShippingRate"
            onChange={this.handleChangeTypeShippingRate}
            value="FlatShippingRate"
          />
          &nbsp; Flat Shipping Rate
        </div>
        <p className="P_last"> Add Comments About Your Order </p>
        <Textarea
          className="form-control boder-text"
          rows="8"
          name="note"
          onChange={this.handleChangeComment}
          validations={[maxLength]}
        />
        <CheckButton
          style={{ display: "none" }}
          ref={c => {
            this.checkBtn = c
          }}
        />
        <button
          type="submit"
          value="submit"
          disabled={!this.state.typeShippingRate}
          className="btn btn-checkout3"
        >
          CONTINUE
        </button>
      </Form>
    )
  }
}

export default Checkout4
