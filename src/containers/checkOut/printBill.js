import React, { Component } from "react";

class PrintBill extends Component {
    constructor(props) {
        super(props);
        console.log('truyen du lieu:', this.props.location);
    }


    render() {
        var data = JSON.stringify(this.props.location.data)
        return (
            <div>
                <p>Print bill</p>
                <div>
                    {data}
                </div>
            </div>
        )
    }
}

export default PrintBill;
