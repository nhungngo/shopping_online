import React, { Component } from 'react'
import { connect } from 'react-redux'
import './contact.scss'
import { http } from '../../services/http.service'
import { Redirect } from 'react-router-dom'
import GoogleMapComponent from '../../components/google-map/google-map.container'
import BreadcrumbComponent from '../../components/breadcrumb/breadcrumb.component'
import OurContactComponent from '../../components/our-contact/our-contact.component'

class ContactComponent extends Component {
  constructor(props) {
    super(props)
  this.state = {
      errorEmail: null,
      isInputValid: true
    }
  }
  submitHandle = e => {
    const form = {
      name: this.state.name,
      email: this.state.email,
      message: this.state.message
    }
    http.post('contacts', form).then(res => {
      window.alert('Success !')
    })
  }
  handleChange = e => {
    let target = e.target
    let name = target.name
    let value = target.value
    this.setState({
      [name]: value
    })
    if (name === 'email') {
      if (value === '') {
        this.setState({
          errorEmail: 'Email is required',
          isInputValid: true
        })
      }
      if (value !== '') {
        const regexEmail = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/
        let match = regexEmail.test(value)
        if (match === false) {
          this.setState({
            errorEmail: 'email is not valid',
            isInputValid: true
          })
        }
        if (match === true) {
          this.setState({
            errorEmail: ''
          })
        }
      }
    }
    if (this.state.errorEmail === '') {
      this.setState({
        isInputValid: false
      })
    }
  }

  render() {
    const routesConfig = [
      {
        path: '/contact',
        breadcrumbName: 'Contact'
      }
    ]
    if (this.props.searchKey !== '') {
      return <Redirect to="/search" />
    }

    return (
      <div className="container">
        {/* BreadCrumb */}
        <div className="bread-crumb">
          <BreadcrumbComponent routesConfig={routesConfig} />
        </div>

        <div className="row">
          {/* Left sidebar */}
          <div className="category-sidebar col-md-3 col-sm-3">
            <OurContactComponent />
          </div>

          {/* Contact form */}
          <div className="col-md-9 col-sm-9 form-group contact-form">
            <p>CONTACT</p>
            <div className="upper-section">
              <div className="google-map-section">
                <GoogleMapComponent />
              </div>

              <p>CONTACT FORM</p>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut
                commodo rhoncus nibh eget tincidunt. Ut sed justo in elit
                gravida dapibus a non libero. Etiam bibendum mollis mi, ut
                lacinia elit convallis eget. Etiam vitae scelerisque turpis, ut
                suscipit ex. Quisque porta urna pellentesque odio pellentesque
                consequat. Nulla scelerisque id turpis ut tincidunt. Proin at
                ornare ipsum. Nam fringilla elit eu magna pretium imperdiet.
              </p>
            </div>

            <div className="lower-section">
              <form onSubmit={this.submitHandle}>
                <div className="form-group">
                  <p>Name</p>
                  <input
                    name="name"
                    type="text"
                    className="form-control form-control-lg"
                    onChange={this.handleChange}
                  />
                </div>
                <div className="form-group">
                  <p>
                    Email <span className="star">*</span>
                  </p>
                  <input
                    name="email"
                    type="email"
                    className="form-control form-control-lg"
                    onChange={this.handleChange}
                    onBlur={this.handleChange}
                  />
                  <div className={this.state.errorEmail ? 'text-danger' : ''}>
                    {this.state.errorEmail}
                  </div>
                </div>
                <div className="form-group">
                  <p>Message</p>
                  <textarea
                    name="message"
                    className="form-control"
                    rows="5"
                    onChange={this.handleChange}
                  />
                </div>

                <button
                  className="btn btn-submit text-uppercase"
                  type="submit"
                  disabled={this.state.isInputValid}
                >
                  submit
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return { searchKey: state.search }
}

export default connect(mapStateToProps)(ContactComponent)
