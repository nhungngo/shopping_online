import React, { Component } from 'react'
import 'antd/dist/antd.css'
import { WrappedAccountForm } from '../../components/account/account.component'

class AccountContainer extends Component {
  render() {
    return (
      <div className="container login">
        <WrappedAccountForm />
      </div>
    )
  }
}

export default AccountContainer
