import React, { Component } from 'react'
import { connect } from 'react-redux'

class PageNotFoundContainer extends Component {
  render() {
    return (
      <h2 className="text-center">Ooooop! Page not found!</h2>
    )
  }
}

export default connect()(PageNotFoundContainer)